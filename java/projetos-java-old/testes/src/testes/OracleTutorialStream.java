package testes;

import testes.entities.Pessoa;

import java.util.Comparator;
import java.util.List;

public class OracleTutorialStream{
  public static void main(String[] args){
    List<Pessoa> pessoas = new Pessoa().populaPessoas();

    var nomes = pessoas.stream().filter(pessoa->pessoa.getNome().startsWith("Juni"));
    nomes.forEach(System.out::println);

    var idades = pessoas.stream()
        .filter(pessoa->pessoa.getNacionaldiade().equals("Brasil"))
        .map(Pessoa::getIdade);
    idades.forEach(System.out::println);

    var nomesAsc = pessoas.stream()
        .filter(pessoa->pessoa.getNacionaldiade().equals("Brasil"))
        .sorted(Comparator.comparing(Pessoa::getNome));
    nomesAsc.forEach(System.out::println);
  }
}
