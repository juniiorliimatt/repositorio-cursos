package com.antpatterns.easybase.repositorio;

import com.antpatterns.easybase.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends CrudRepository<User, String>, UsuarioCustomRepository {
}
