package thirtydaysofcode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Result {
  public static void solve(double mealCost, double tipPercent, double taxPercent) {
    double tip = mealCost * tipPercent / 100;
    double tax = mealCost * taxPercent / 100;
    int totalCost = (int)Math.round( mealCost + tip + tax);
    System.out.println(totalCost);
  }
}

public class DayTwo {
  public static void main(String[] args) throws IOException {
    var bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    var mealCost = Double.parseDouble(bufferedReader.readLine().trim());

    var tipPercent = Double.parseDouble(bufferedReader.readLine().trim());

    var taxPercent = Double.parseDouble(bufferedReader.readLine().trim());

    Result.solve(mealCost, tipPercent, taxPercent);

    bufferedReader.close();
  }
}
