import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CalculadoraTest{

  @Test
  void deveriaSomarDoisNumerosPositivos(){
    Calculadora calc = new Calculadora();
    int soma = calc.soma(3,6);
    Assertions.assertEquals(10, soma);
  }
}
