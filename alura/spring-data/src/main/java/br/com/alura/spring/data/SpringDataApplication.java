package br.com.alura.spring.data;

import br.com.alura.spring.data.crud.Crud;
import br.com.alura.spring.data.service.CrudCargoService;
import br.com.alura.spring.data.service.CrudFuncionarioService;
import br.com.alura.spring.data.service.CrudUnidadeDeTrabalhoService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataApplication implements CommandLineRunner{
  
  private final CrudCargoService crudCargoService;
  private final CrudFuncionarioService crudFuncionarioService;
  private final CrudUnidadeDeTrabalhoService crudUnidadeDeTrabalhoService;
  
  public SpringDataApplication(CrudCargoService crudCargoService,CrudFuncionarioService crudFuncionarioService,
                               CrudUnidadeDeTrabalhoService crudUnidadeDeTrabalhoService){
    this.crudCargoService=crudCargoService;
    this.crudFuncionarioService=crudFuncionarioService;
    this.crudUnidadeDeTrabalhoService=crudUnidadeDeTrabalhoService;
  }
  
  public static void main(String[] args){
    
    SpringApplication.run(SpringDataApplication.class,args);
  }
  
  @Override
  public void run(String... args) throws Exception{
    Crud crud = new Crud(crudCargoService,crudFuncionarioService,crudUnidadeDeTrabalhoService);
    crud.escolherOrm();
  }
}
