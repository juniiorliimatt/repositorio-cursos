package br.com.alura.escola.dominio.aluno;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TelefoneTest{

	@Test
	void naoDeveriaCriarTelefoneComNumeroInvalido(){
		Assertions.assertThrows(IllegalArgumentException.class, ()->new Telefone(null, null));
		Assertions.assertThrows(IllegalArgumentException.class, ()->new Telefone(null, "981568007"));
		Assertions.assertThrows(IllegalArgumentException.class, ()->new Telefone("085", null));
		Assertions.assertThrows(IllegalArgumentException.class, ()->new Telefone("888", "9999999999"));
	}

	@Test
	void deveriaCriarUmTelefoneComPadraoValido(){
		Assertions.assertDoesNotThrow(()->new Telefone("85", "981568007"));
	}
}
