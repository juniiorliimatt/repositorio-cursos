package br.com.alura.loja.modelo;

import com.thoughtworks.xstream.XStream;

public class Projeto{

	private Long id;
	private String nome;
	private int ano;

	public Projeto(){
	}

	public Projeto(Long id, String nome, int ano){
		this.id=id;
		this.nome=nome;
		this.ano=ano;
	}

	public Long getId(){
		return id;
	}

	public void setId(Long id){
		this.id=id;
	}

	public String getNome(){
		return nome;
	}

	public void setNome(String nome){
		this.nome=nome;
	}

	public int getAno(){
		return ano;
	}

	public void setAno(int ano){
		this.ano=ano;
	}

	public String toXML(){
		return new XStream().toXML(this);
	}
}
