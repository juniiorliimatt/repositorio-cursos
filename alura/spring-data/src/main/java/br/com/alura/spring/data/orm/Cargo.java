package br.com.alura.spring.data.orm;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="cargos")
public class Cargo{
  
  @Id
  @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CARGO_SEQUENCE")
  @SequenceGenerator(sequenceName="cargo_seq", allocationSize=1, name="CARGO_SEQUENCE")
  private Integer id;
  private String descricao;
  
  @OneToMany(mappedBy="cargo")
  private List<Funcionario> funcionarios=new ArrayList<>();
  
  public Cargo(){
  }
  
  public Cargo(String descricao){
    this.descricao=descricao;
  }
  
  public Cargo(Integer id,String descricao){
    this.id=id;
    this.descricao=descricao;
  }
  
  public Integer getId(){
    return id;
  }
  
  public void setId(Integer id){
    this.id=id;
  }
  
  public String getDescricao(){
    return descricao;
  }
  
  public void setDescricao(String descricao){
    this.descricao=descricao;
  }
  
  public List<Funcionario> getFuncionarios(){
    return funcionarios;
  }
  
  @Override
  public String toString(){
    return "Cargo id: "+id+", descricao: "+descricao+".";
  }
}
