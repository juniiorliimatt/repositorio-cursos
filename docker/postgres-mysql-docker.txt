docker run --name postgresql-server -e "POSTGRES_PASSWORD=12345" -p 5432:5432 -d postgres

docker run --name mysql-server -e MYSQL_ROOT_PASSWORD=12345 -p 3306:3306 -d mysql
mysql> CREATE USER 'root'@'%' IDENTIFIED BY 'PASSWORD';
mysql> GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
mysql> FLUSH PRIVILEGES;


CREATE USER IF NOT EXISTS mysqluser IDENTIFIED BY '12345';

grant all privileges on mysql.* to 'mysql'@'localhost';



docker run --name oracle-server -p 1521:1521 -d store/oracle/database-enterprise:12.2.0.1