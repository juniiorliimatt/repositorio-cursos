package br.com.alura.spring.data.service;

import br.com.alura.spring.data.orm.Cargo;
import br.com.alura.spring.data.repository.CargoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Scanner;

@Service
public class CrudCargoService{
  private final CargoRepository repository;
  
  @Autowired
  public CrudCargoService(CargoRepository repository){
    this.repository=repository;
  }
  Scanner scanner=new Scanner(System.in);
  
  public void salvar(){
    System.out.println("\nDescrição do cargo");
    repository.save(new Cargo(scanner.next()));
    System.out.println("Salvo!");
    scanner.close();
  }
  
  public void atualizar(){
    System.out.println("\nQual o id do cargo que desseja atualizar?");
    Integer id=scanner.nextInt();
    System.out.println("Descrição do cargo:");
    String descricao=scanner.next();
    repository.save(new Cargo(id,descricao));
    System.out.println("Atualizado");
    scanner.close();
  }
  
  public void listarTodos(){
    System.out.println();
    Iterable<Cargo> cargos=repository.findAll();
    System.out.println();
    cargos.forEach(System.out::println);
    System.out.println("Fim da lista");
    scanner.close();
  }
  
  public void deletar(){
    System.out.println("\nQual cargo desseja deletar?");
    repository.deleteById(scanner.nextInt());
    System.out.println("Deletado");
    scanner.close();
  }
  
  public void deletarTodos(){
    System.out.println();
    repository.deleteAll();
    System.out.println("Deletados");
    scanner.close();
  }
}
