package tests;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class TestaInsercao{
  public static void main(String[] args) throws SQLException{
    ConnectionFactory factory=new ConnectionFactory();
    Connection connection=factory.getConnection();
    
    PreparedStatement statement=connection.prepareStatement("INSERT INTO PRODUTO(NOME, DESCRICAO) VALUES (?,?)",
      Statement.RETURN_GENERATED_KEYS);
    
    statement.setString(1,"Notebook");
    statement.setString(2,"Notebook Sansung");
    
    statement.close();
    connection.close();
  }
}
