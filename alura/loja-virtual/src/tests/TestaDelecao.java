package tests;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TestaDelecao{
  public static void main(String[] args) throws SQLException{
    ConnectionFactory factory=new ConnectionFactory();
    Connection connection=factory.getConnection();
    
    PreparedStatement statement=connection.prepareStatement("DELETE FROM PRODUTO");
    statement.execute();
    
    statement.close();
    connection.close();
  }
}
