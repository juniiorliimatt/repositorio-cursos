package valida.cpf;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

public class Cpf {
	final Logger log = Logger.getLogger(Cpf.class.getName());

	private Cpf() {
	}

	public static String valida(Scanner scan, Logger log) {
		var cpf = get(scan, log);

		List<Integer> numerosCpf = toArrayInteger(cpf);
		List<Integer> calculoUm = Calcula.digitoVerificadorUm(numerosCpf);
		var somaCalculoUm = Calcula.soma(calculoUm);
		var digitoUm = Verifica.digito(somaCalculoUm);

		List<Integer> calculoDois = Calcula.digitoVerificadorDois(numerosCpf, digitoUm);
		var somaCalculoDois = Calcula.soma(calculoDois);
		var digitoDois = Verifica.digito(somaCalculoDois);
		return "CPF: " + cpf + "-" + digitoUm + digitoDois + "\n\n";
	}

	private static String get(Scanner scan, Logger log) {
		log.info("Entre com o CPF sem o digito: ");
		var cpf = scan.next();
		if (cpf.length() > 9) {
			log.info("CPF inválido! A quantidade de digitos é maior do que o esperado.");
			get(scan, log);
		}
		return cpf;
	}

	private static List<Integer> toArrayInteger(String cpf) {
		List<Integer> numerosCpf = new ArrayList<>();
		for (var i = 0; i < cpf.length(); i++) {
			var c = cpf.charAt(i);
			numerosCpf.add(Integer.parseInt(Character.toString(c)));
		}
		return numerosCpf;
	}
}
