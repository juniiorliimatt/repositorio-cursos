# provider "aws" {
#   region = "us-east-1"
# }

# provider "aws" {
#   alias = "us-east-2"
#   region = "us-east-2"
# }

# resource "aws_instance" "developer" {
#   count = 3
#   ami = var.amis["us-east-1"]
#   instance_type = "t2.micro"
#   key_name = var.key_name
#   tags = {
#     Name = "developer${count.index}"
#   }
#   vpc_security_group_ids = ["${aws_security_group.acesso-ssh.id}"]
# }

# /*
# resource "aws_instance" "developer4" {
#   ami = var.amis["us-east-1"]
#   instance_type = "t2.micro"
#   key_name = var.key_name
#   tags = {
#     Name = "developer4"
#   }
#   vpc_security_group_ids = ["${aws_security_group.acesso-ssh.id}"]
#   depends_on = [
#     aws_s3_bucket.developer4
#   ]
# }
# */

# resource "aws_instance" "developer5" {
#   ami = var.amis["us-east-1"]
#   instance_type = "t2.micro"
#   key_name = var.key_name
#   tags = {
#     Name = "developer5"
#   }
#   vpc_security_group_ids = ["${aws_security_group.acesso-ssh.id}"]
# }

# resource "aws_instance" "developer6" {
#   provider = aws.us-east-2
#   ami = var.amis["us-east-2"]
#   instance_type = "t2.micro"
#   key_name = var.key_name
#   tags = {
#     Name = "developer6"
#   }
#   vpc_security_group_ids = ["${aws_security_group.acesso-ssh-us-east-2.id}"]
#   depends_on = [
#     aws_dynamodb_table.dynamodb-homologacao
#   ]
# }

# /*
# resource "aws_s3_bucket" "developer4" {
#   bucket = "rmerceslabs-developer4"
#   acl = "private"

#   tags = {
#     Name = "rmerceslabs-developer4"
#   }
# }
# */

# resource "aws_dynamodb_table" "dynamodb-homologacao" {
#   provider = aws.us-east-2
#   name           = "GameScores"
#   billing_mode   = "PAY_PER_REQUEST"
#   hash_key       = "UserId"
#   range_key      = "GameTitle"

#   attribute {
#     name = "UserId"
#     type = "S"
#   }

#   attribute {
#     name = "GameTitle"
#     type = "S"
#   }
# }

# resource "aws_instance" "developer7" {
#   provider = aws.us-east-2
#   ami = "${var.amis["us-east-2"]}"
#   instance_type = "t2.micro"
#   key_name = "${var.key_name}"
#   tags = {
#     Name = "developer7"
#   }
#   vpc_security_group_ids = ["${aws_security_group.acesso-ssh-us-east-2.id}"]
# }