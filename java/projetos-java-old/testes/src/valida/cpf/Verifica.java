package valida.cpf;

import java.util.logging.Logger;

public class Verifica {
	final Logger log = Logger.getLogger(Verifica.class.getName());

	private Verifica() {
	}

	public static Integer digito(Integer somaCalculoUm) {
		if ((somaCalculoUm % 11) < 2) {
			return 0;
		}
		return 11 - (somaCalculoUm % 11);
	}
}
