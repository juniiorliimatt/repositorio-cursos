# Observações ORACLE

## Instalação

### Oracle SQL Express Edition

Necessário ter uma conta Oracle.

[https://www.oracle.com/br/database/technologies/appdev/xe.html](https://www.oracle.com/br/database/technologies/appdev/xe.html)

No windows, seguir o processo de instalação normal, fornecendo senha, e anotando os usuários disponíveis, no final da instalação ele vai exibir o banco de dados padrão, anotar tudo.

### Oracle Sqldeveloper

[https://www.oracle.com/tools/downloads/sqldev-downloads.html](https://www.oracle.com/tools/downloads/sqldev-downloads.html)

Dar preferência a versão já com o JDK instalado

Descompactar em alguma pasta, e criar um atalho.

host: [localhost](http://localhost)

port: 1521

database: XE

service name: XEPDB1

Autentication database native

username SYSTEM

role: normal

password: 12345

### Usar a ferramenta Assistende de conexão de Rede para definir a conexao

### Usando sqlplus

```powershell
sqlplus system/<senha>@nome_conexão
# sqlplus system/12345@XEPDB1
```

## Tipos de conexão

Existem dois tipos de conexão

- Conexão ao banco de dados plugável onde o serviço é XEDPB1
    - Apenas o usuário system tem acesso total a esse serviço.
- Conexão ao banco de dados onde o serviço é XE
    - Apenas o usuário sys tem acesso total a esse serviço

Ambos usam a senha que foi definida na instalação do Oracle Database. 

- oracle

Abrir sqldeveloper e criar uma primeira conexão com o banco, na tela de conexão por o nome do serviço definir um padrão usuario_SERVIÇO informar o login SYSTEM do tipo SYSDBA e a senha que foi definida no momento da instalação do banco, definir o host, porta e nome do serviço ou SID, testar a conexão,  salvar e conectar.

```markdown
serviço: XEPDB1 

system/system

sys/system

hr/hr
```

Após conectar, liberar o usuário hr:

```sql
ALTER USER hr ACCOUNT UNLOCK;
```

Feito, alterar a senha do usuário hr:

```sql
ALTER USER hr IDENTIFIED BY hr;
```

Após, criar uma nova conexão com o banco usando o usuário HR, definir como usuário padrão, informar senha, e informações de conexão.

## Criando um usuário

Logue-se como um usuário sysdba.

```sql
CREATE USER usuario IDENTIFIED BY Oracle123;
GRANT CREATE SESSION TO usuario ;
GRANT CREATE TABLE TO usuario ;

GRANT CREATE VIEW TO usuario ;
GRANT CREATE DATABASE LINK TO usuario ;
GRANT CREATE ANY PROCEDURE TO usuario ;
GRANT UNLIMITED TABLESPACE to usuario ;
GRANT SELECT ANY TABLE to usuario ;
GRANT UPDATE ANY TABLE to usuario ;
GRANT INSERT ANY TABLE to usuario ;
GRANT DROP ANY TABLE to usuario ;
GRANT ALTER ANY SEQUENCE TO usuario ;

-- Caso não consiga os privilégios necessários
GRANT ALL PRIVILEGES to usuario ;

SELECT * FROM DBA_SYS_PRIVS WHERE GRANTEE = 'usuarioemmaiusculo';
```