package br.com.alura.tdd.service;

import br.com.alura.tdd.modelo.Desempenho;
import br.com.alura.tdd.modelo.Funcionario;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

class ReajusteServiceTest{

  @Test
  void reajusteDeveriaSerDeTresPorcentoQuandoODesempenhoForADesejar(){
    ReajusteService service=new ReajusteService();
    Funcionario funcionario=new Funcionario("Teste", LocalDate.now(), new BigDecimal("1000"));
    service.concederReajuste(funcionario, Desempenho.A_DESEJAR);
    Assertions.assertEquals(new BigDecimal("1030.00"), funcionario.getSalario());
  }

  @Test
  void reajusteDeveriaSerDeQuinzePorcentoQuandoODesempenhoForBom(){
    ReajusteService service=new ReajusteService();
    Funcionario funcionario=new Funcionario("Teste", LocalDate.now(), new BigDecimal("1000"));
    service.concederReajuste(funcionario, Desempenho.BOM);
    Assertions.assertEquals(new BigDecimal("1150.00"), funcionario.getSalario());
  }

  @Test
  void reajusteDeveriaSerDeVintePorcentoQuandoODesempenhoForBom(){
    ReajusteService service=new ReajusteService();
    Funcionario funcionario=new Funcionario("Teste", LocalDate.now(), new BigDecimal("1000"));
    service.concederReajuste(funcionario, Desempenho.OTIMO);
    Assertions.assertEquals(new BigDecimal("1200.00"), funcionario.getSalario());
  }

  @Test
  void reajusteDeveriaSerDeQuarentaPorcentoQuandoODesempenhoForEspetacular(){
    ReajusteService service=new ReajusteService();
    Funcionario funcionario=new Funcionario("Teste", LocalDate.now(), new BigDecimal("1000"));
    service.concederReajuste(funcionario, Desempenho.ESPETACULAR);
    Assertions.assertEquals(new BigDecimal("1400.00"), funcionario.getSalario());
  }
}
