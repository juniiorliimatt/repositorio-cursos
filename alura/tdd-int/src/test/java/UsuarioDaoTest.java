import br.com.alura.leilao.dao.UsuarioDao;
import br.com.alura.leilao.model.Usuario;
import br.com.alura.leilao.util.JPAUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

class UsuarioDaoTest{

  private EntityManager em;
  private UsuarioDao dao;

  @BeforeEach
  public void beforeEach(){
    this.em=JPAUtil.getEntityManager();
    this.dao=new UsuarioDao(em);
    em.getTransaction().begin();
  }

  @AfterEach
  public void afterEach(){
    em.getTransaction().rollback();
  }

  private Usuario criaUsuario(){
    Usuario usuario=new Usuario("fulano", "fulano@email.com", "12345678");
    em.persist(usuario);
    return usuario;
  }

  @Test
  void deveriaEncontrarUsuarioCadastrado(){
    Assertions.assertNotNull(this.dao.buscarPorUsername(criaUsuario().getNome()));
  }

  @Test
  void naoDeveriaEncontrarUsuarioNaoCadastrrado(){
    Assertions.assertThrows(NoResultException.class, ()->this.dao.buscarPorUsername("beltrano"));
  }

  @Test
  void deveriaRemoverUmUsuario(){
    Usuario usuario=criaUsuario();
    this.dao.deletar(usuario);
    Assertions.assertThrows(NoResultException.class, ()->this.dao.buscarPorUsername(usuario.getNome()));
  }
}
