package br.com.alura.leilao.leilao;

import br.com.alura.leilao.login.LoginPage;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

class LeilaoTest{

  private LeilaoPage leilaoPage;
  private CadastroLeilaoPage cadastroLeilaoPage;

  @BeforeEach
  void beforeEach(){
    LoginPage loginPage=new LoginPage();
    loginPage.preencheFormularioDeLogin("fulano", "pass");
    this.leilaoPage=loginPage.efetuarLogin();
    this.cadastroLeilaoPage = leilaoPage.carregarFormulario();
  }

  @AfterEach
  void afterEach(){
    this.leilaoPage.fecharBrowser();
  }

  @Test
  void deveriaCadastrarLeilao(){
    String hoje = LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    String nome = "Leilão do dia " + hoje;
    String valor = "500.00";

    this.leilaoPage = cadastroLeilaoPage.cadastrarLeilao(nome, valor, hoje);
    Assertions.assertTrue(leilaoPage.isLeilaoCadastrado(nome, valor, hoje));
  }

  @Test
  void deveriaValidarCadasstroDeLeilao(){
    this.leilaoPage = cadastroLeilaoPage.cadastrarLeilao("", "", "");
    Assertions.assertFalse(this.cadastroLeilaoPage.isPaginaAtual());
    Assertions.assertTrue(this.leilaoPage.isPaginaAtual());
    Assertions.assertTrue(this.cadastroLeilaoPage.isMensagemDeValidacaoVisiveis());
  }
}
