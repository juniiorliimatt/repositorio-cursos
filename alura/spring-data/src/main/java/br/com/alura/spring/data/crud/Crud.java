package br.com.alura.spring.data.crud;

import br.com.alura.spring.data.service.CrudCargoService;
import br.com.alura.spring.data.service.CrudFuncionarioService;
import br.com.alura.spring.data.service.CrudUnidadeDeTrabalhoService;

import java.util.Scanner;

public class Crud{
  private boolean system=true;
  private final CrudCargoService crudCargoService;
  private final CrudFuncionarioService crudFuncionarioService;
  private final CrudUnidadeDeTrabalhoService crudUnidadeDeTrabalhoService;
  private static final String SAIR="0 - Sair";
  private static final String ACTION="Qual ação você quer executar? ";
  private final String VOLTAR = "6 - Voltar";
  
  public Crud(CrudCargoService crudCargoService,CrudFuncionarioService crudFuncionarioService,
              CrudUnidadeDeTrabalhoService crudUnidadeDeTrabalhoService){
    this.crudCargoService=crudCargoService;
    this.crudFuncionarioService=crudFuncionarioService;
    this.crudUnidadeDeTrabalhoService=crudUnidadeDeTrabalhoService;
  }
  
  public void escolherOrm(){
    Scanner scanner=new Scanner(System.in);
    
    System.out.println("1 - Unidade de Trabalho");
    System.out.println("2 - Funcionario");
    System.out.println("3 - Cargo de Trabalho");
    
    System.out.print("Qual ORM vc deseja trabalhar? ");
    Integer ormEscolhido=scanner.nextInt();
    
    switch(ormEscolhido){
      case 1:
        crudUnidadeDeTrabalho(crudUnidadeDeTrabalhoService,scanner);
        break;
      case 2:
        crudFuncionario(crudFuncionarioService,scanner);
        break;
      case 3:
        crudCargo(crudCargoService,scanner);
        break;
      default:
        System.out.println("Opção inválida");
    }
    
    scanner.close();
  }
  
  private void crudUnidadeDeTrabalho(CrudUnidadeDeTrabalhoService crudUnidadeDeTrabalhoService,Scanner scanner){
    while(system){
      System.out.println();
      System.out.println(SAIR);
      System.out.println("1 - Nova Unidade de trabalho");
      System.out.println("2 - Atualizar uma Unidade de trabalho");
      System.out.println("3 - Deletar uma Unidade de Trabalho");
      System.out.println("4 - Listar todas as Unidades de trabalho");
      System.out.println("5 - Deletar todas as Unidades de trabalho");
      System.out.println(VOLTAR);
      
      System.out.print(ACTION);
      int action=scanner.nextInt();
      
      switch(action){
        case 0:
          system=false;
          break;
        case 1:
          crudUnidadeDeTrabalhoService.salvar(scanner);
          break;
        case 2:
          crudUnidadeDeTrabalhoService.atualizar(scanner);
          break;
        case 3:
          crudUnidadeDeTrabalhoService.deletar(scanner);
          break;
        case 4:
          crudUnidadeDeTrabalhoService.listarTodos(scanner);
          break;
        case 5:
          crudUnidadeDeTrabalhoService.deletarTodos(scanner);
          break;
        case 6:
          Crud crud = new Crud(crudCargoService,crudFuncionarioService,crudUnidadeDeTrabalhoService);
          crud.escolherOrm();
          break;
        default:
          System.out.println("Ação inválida");
      }
      System.out.println();
    }
  }
  
  private void crudCargo(CrudCargoService crudCargoService,Scanner scanner){
    while(system){
      System.out.println();
      System.out.println(SAIR);
      System.out.println("1 - Novo Cargo");
      System.out.println("2 - Atualizar um cargo");
      System.out.println("3 - Deletar um cargo");
      System.out.println("4 - Listar todos os cargos");
      System.out.println("5 - Deletar todos os cargos");
      System.out.println(VOLTAR);
      
      System.out.print(ACTION);
      int action=scanner.nextInt();
      
      switch(action){
        case 0:
          system=false;
          break;
        case 1:
          crudCargoService.salvar();
          break;
        case 2:
          crudCargoService.atualizar();
          break;
        case 3:
          crudCargoService.deletar();
          break;
        case 4:
          crudCargoService.listarTodos();
          break;
        case 5:
          crudCargoService.deletarTodos();
          break;
        case 6:
          Crud crud = new Crud(crudCargoService,crudFuncionarioService,crudUnidadeDeTrabalhoService);
          crud.escolherOrm();
          break;
        default:
          System.out.println("Ação inválida.");
      }
      
      System.out.println();
    }
  }
  
  private void crudFuncionario(CrudFuncionarioService crudFuncionarioService,Scanner scanner){
    while(system){
      System.out.println();
      System.out.println(SAIR);
      System.out.println("1 - Novo Funcionario");
      System.out.println("2 - Atualizar Funcionario");
      System.out.println("3 - Deletar Funcionario");
      System.out.println("4 - Listar todos Funcionarios");
      System.out.println("5 - Deletar todas Funcionarios");
      System.out.println(VOLTAR);
      
      System.out.print(ACTION);
      int action=scanner.nextInt();
      
      switch(action){
        case 0:
          system=false;
          break;
        case 1:
          crudFuncionarioService.salvar();
          break;
        case 2:
          crudFuncionarioService.atualizar();
          break;
        case 3:
          crudFuncionarioService.deletar();
          break;
        case 4:
          crudFuncionarioService.listarTodos();
          break;
        case 5:
          crudFuncionarioService.deletarTodos();
          break;
        case 6:
          Crud crud = new Crud(crudCargoService,crudFuncionarioService,crudUnidadeDeTrabalhoService);
          crud.escolherOrm();
          break;
        default:
          System.out.println("Ação inválida");
      }
      System.out.println();
    }
  }
}
