package javaoito;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static java.util.Comparator.comparing;

public class OrdenaStrings{
  public static void main(String[] args){
    List<String> palavras=new ArrayList<>();

    palavras.add("A");
    palavras.add("AB");
    palavras.add("ABC");

    palavras.sort(Comparator.comparingInt(String::length));
    palavras.forEach(palavra->System.out.println(palavra));

    palavras.sort(comparing(String::length));
    palavras.forEach(System.out::println);

    palavras.sort(comparing(String::length));
    palavras.forEach(System.out::println);
  }
}