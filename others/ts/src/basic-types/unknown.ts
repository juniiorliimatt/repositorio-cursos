// uma espécie de tipo any, porém mais seguro.

let x: unknown;
x = 100;
x = 'Junior';
x = 900;
x = '10';
const y = 800;
console.log(x + y);
