package testes;

import java.util.ArrayList;
import java.util.List;

public class StringToArray {

    public static void main(String[] args) {

        List<Integer> numeros = new ArrayList<>();
        String cpf = "035774883";
        
        for(int i = 0; i < cpf.length(); i++) {
            char c = cpf.charAt(i);
            numeros.add(Integer.parseInt(Character.toString(c)));
        }
    }
}
