package application;

import chess.ChessMatch;

public class Program {
  public static void main(String[] args) {
    // TODO acompanahar pelo github https://github.com/acenelio/chess-system-java
    var chessMatch = new ChessMatch();
    UI.printBoard(chessMatch.getPieces());
  }
}
