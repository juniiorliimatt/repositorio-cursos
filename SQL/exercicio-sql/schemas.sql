CREATE TABLE EMPREGADOS (
    matricula NVARCHAR2(6) PRIMARY KEY,
    nome NVARCHAR2(100) NOT NULL,
    sobrenome NVARCHAR2(100) NOT NULL,
    departamento NVARCHAR2(3),
    telefone NVARCHAR2(14),
    data_admissao DATE,
    cargo NVARCHAR2(10),
    niveled NUMBER,
    sexo NVARCHAR2(1),
    data_nascimento DATE,
    salario NUMBER(9,2),
    bonus NUMBER(9,2),
    comissao NUMBER(9,2)
);

CREATE TABLE DEPARTAMENTOS (
    codigo_departamento NVARCHAR2(3) PRIMARY KEY,
    nome_departamento NVARCHAR2(100) NOT NULL,
    gerente NVARCHAR2(6),
    departamento_subordinado_a NVARCHAR2(3)
);

CREATE TABLE PROJETOS (
    codigo_projeto NVARCHAR2(6) PRIMARY KEY,
    nome_projeto NVARCHAR2(24) NOT NULL,
    codigo_departamento NVARCHAR2(3) NOT NULL,
    matricula_responsavel NVARCHAR2(6) NOT NULL,
    equipe NUMBER(5),
    data_inicio DATE,
    data_fim DATE,
    projeto_subordinado_a NVARCHAR2(6)
);