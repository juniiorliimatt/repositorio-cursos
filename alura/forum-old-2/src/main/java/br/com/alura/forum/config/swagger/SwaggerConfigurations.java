package br.com.alura.forum.config.swagger;

import br.com.alura.forum.modelo.Usuario;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ScalarType;
import springfox.documentation.service.ParameterType;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.time.LocalDate;

import static java.util.Collections.singletonList;

@Configuration
public class SwaggerConfigurations{

  @Bean
  public Docket forumApi(){
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.basePackage("br.com.alura.forum"))
        .paths(PathSelectors.any())
        .build()
        .pathMapping("/")
        .directModelSubstitute(LocalDate.class, String.class)
        .genericModelSubstitutes(ResponseEntity.class)
        .ignoredParameterTypes(Usuario.class)
        .globalRequestParameters(singletonList(new springfox.documentation.builders.RequestParameterBuilder()
                                                   .name("Authorization")
                                                   .description("Header para token JWT")
                                                   .in(ParameterType.QUERY)
                                                   .required(false)
                                                   .query(q->q.model(m->m.scalarModel(ScalarType.STRING)))
                                                   .build()));
  }
}
