// um nome para o tipo

type Idade = number;
type Pessoa = {
  nome: string;
  idade: Idade;
  salario: number;
  corPreferida: string;
};

type CorRGB = 'VERMELHO' | 'VERDE' | 'AZUL';
type CorCMYK = 'CIANO' | 'MAGENTA' | 'AMARELO' | 'PRETO';
type corPreferida = CorRGB | CorCMYK;

const pessoa: Pessoa = {
  nome: 'Junior',
  idade: 30,
  salario: 200000,
};

export function setCorPreferida(pessoa: Pessoa, cor: corPreferida): Pessoa {
  return { ...pessoa, corPreferida: cor };
}

console.log(setCorPreferida(pessoa, 'AZUL'));
console.log(pessoa);
