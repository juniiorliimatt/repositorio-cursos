package br.com.alura.loja.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name="itens_pedido")
public class ItemPedido{
  
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;
  
  private BigDecimal precoUnitario;
  private Integer quantidade;
  
  @ManyToOne
  private Produto produto;
  
  @ManyToOne
  private Pedido pedido;
  
  public ItemPedido(){
  }
  
  public ItemPedido(Integer quantidade,Produto produto,Pedido pedido){
    this.quantidade=quantidade;
    this.produto=produto;
    this.precoUnitario = produto.getPreco();
    this.pedido=pedido;
  }
  
  public Long getId(){
    return id;
  }
  
  public void setId(Long id){
    this.id=id;
  }
  
  public BigDecimal getPrecoUnitario(){
    return precoUnitario;
  }
  
  public void setPrecoUnitario(BigDecimal precoUnitario){
    this.precoUnitario=precoUnitario;
  }
  
  public Integer getQuantidade(){
    return quantidade;
  }
  
  public void setQuantidade(Integer quantidade){
    this.quantidade=quantidade;
  }
  
  public Produto getProduto(){
    return produto;
  }
  
  public void setProduto(Produto produto){
    this.produto=produto;
  }
  
  public Pedido getPedido(){
    return pedido;
  }
  
  public void setPedido(Pedido pedido){
    this.pedido=pedido;
  }
}
