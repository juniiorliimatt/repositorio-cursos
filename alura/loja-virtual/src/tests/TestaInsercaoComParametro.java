package tests;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestaInsercaoComParametro{
  public static void main(String[] args) throws SQLException{
    ConnectionFactory factory=new ConnectionFactory();
    Connection connection=factory.getConnection();
    
    String nome="Mouse";
    String descricao="Mouse Sem Fio";
    
    PreparedStatement pstm=connection.prepareStatement("INSERT INTO PRODUTO(NOME, DESCRICAO) VALUES (?, ?)",
      Statement.RETURN_GENERATED_KEYS);
    
    pstm.setString(1,nome);
    pstm.setString(2,descricao);
    
    pstm.execute();
  }
}
