package thirtydaysofcode;

import java.util.Scanner;

public class DayThree{
  public static void main(String[] args){
    var sc = new Scanner(System.in);
    var n = sc.nextInt();

    if(!(n%2==0)){
      System.out.println("Weird");
    }else if(n>1&&n<6){
      System.out.println("Not Weird");
    }else if(n>5&&n<21){
      System.out.println("Weird");
    }else{
      System.out.println("Not Weird");
    }

    sc.close();
  }
}
