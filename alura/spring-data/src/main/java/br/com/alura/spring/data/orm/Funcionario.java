package br.com.alura.spring.data.orm;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name="funcionarios")
public class Funcionario{
  
  @Id
  @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FUNCIONARIO_SEQUENCE")
  @SequenceGenerator(sequenceName="funcionario_seq", allocationSize=1, name="FUNCIONARIO_SEQUENCE")
  private Integer id;
  private String nome;
  private String cpf;
  private BigDecimal salario;
  
  @Column(name="data_contratacao")
  private LocalDate dataContratacao;
  
  @ManyToOne
  @JoinColumn(name="cargo_id", nullable=false)
  private Cargo cargo;
  
  @Fetch(FetchMode.SELECT)
  @ManyToMany(fetch=FetchType.EAGER)
  @JoinTable(name="funcionario_unidade",
    joinColumns={
      @JoinColumn(name="fk_funcionario")},
    inverseJoinColumns={
      @JoinColumn(name="fk_unidade")})
  private List<UnidadeDeTrabalho> unidadeDeTrabalho;
  
  public Funcionario(){
  }
  
  public Funcionario(String nome,String cpf,BigDecimal salario,LocalDate dataContratacao){
    this.nome=nome;
    this.cpf=cpf;
    this.salario=salario;
    this.dataContratacao=dataContratacao;
  }
  
  public Funcionario(Integer id,String nome,String cpf,BigDecimal salario,LocalDate dataContratacao){
    this.id=id;
    this.nome=nome;
    this.cpf=cpf;
    this.salario=salario;
    this.dataContratacao=dataContratacao;
  }
  
  public Integer getId(){
    return id;
  }
  
  public String getNome(){
    return nome;
  }
  
  public String getCpf(){
    return cpf;
  }
  
  public BigDecimal getSalario(){
    return salario;
  }
  
  public LocalDate getDataContratacao(){
    return dataContratacao;
  }
  
  @Override
  public String toString(){
    return "Funcionario{"+"id="+id+", nome='"+nome+'\''+", cpf='"+cpf+'\''+", salario="+salario+", dataContratacao="+dataContratacao+'}';
  }
}
