package testes.entities;

import java.util.Arrays;
import java.util.List;

public class Pessoa{
  private Integer id;
  private String nome;
  private String nacionaldiade;
  private Integer idade;

  public Pessoa(){
  }

  public Pessoa(Integer id,String nome,String nacionaldiade,Integer idade){
    this.id = id;
    this.nome = nome;
    this.nacionaldiade = nacionaldiade;
    this.idade = idade;
  }

  public Integer getId(){
    return id;
  }

  public String getNome(){
    return nome;
  }

  public String getNacionaldiade(){
    return nacionaldiade;
  }

  public Integer getIdade(){
    return idade;
  }

  public List<Pessoa> populaPessoas(){
    return Arrays.asList(new Pessoa(1,"Matheus Henrique","Brasil",18),
                         new Pessoa(2,"Hernandez Roja","Brasil",21),
                         new Pessoa(3,"Damyres Maciel","Brasil",22),
                         new Pessoa(4,"Junior Lima","Brasil",22));
  }

  @Override
  public String toString(){
    return "nome: "+nome;
  }
}
