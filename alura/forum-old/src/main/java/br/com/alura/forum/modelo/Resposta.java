package br.com.alura.forum.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
public class Resposta{
  
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;
  private String mensagem;
  
  @ManyToOne
  private Topico topico;
  private LocalDateTime dataCriacao=LocalDateTime.now();
  
  @ManyToOne
  private Usuario autor;
  private Boolean solucao=false;
  
  public Long getId(){
    return id;
  }
  
  public String getMensagem(){
    return mensagem;
  }
  
  public Topico getTopico(){
    return topico;
  }
  
  public LocalDateTime getDataCriacao(){
    return dataCriacao;
  }
  
  public Usuario getAutor(){
    return autor;
  }
  
  public Boolean getSolucao(){
    return solucao;
  }
  
  @Override
  public boolean equals(Object o){
    if(this==o) return true;
    if(o==null || getClass()!=o.getClass()) return false;
    Resposta resposta=(Resposta) o;
    return Objects.equals(id,resposta.id);
  }
  
  @Override
  public int hashCode(){
    return Objects.hash(id);
  }
}
