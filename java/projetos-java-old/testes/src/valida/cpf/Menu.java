package valida.cpf;

import java.util.Scanner;
import java.util.logging.Logger;

public class Menu {
	static final Logger log = Logger.getLogger(Menu.class.getName());

	private Menu() {
	}

	public static void opcoes() {
		var scan = new Scanner(System.in);
		var continuar = true;
		var dev = Desenvolvedor.obterDados(scan, log);

		while (continuar) {
			log.info("\n\nO que vc deseja fazer? \n" + "Opção 1 - Verificar o CPF \n" + "Opção 2 - Dados do desenvolvedor \n"
					+ "Digite qualquer letra + enter para sair \n");
			log.info("\nDigite a opção escolhida: ");
			String opcao = scan.next();

			switch (opcao) {
				case "1":
					Cpf.valida(scan, log);
					break;
				case "2":
					Desenvolvedor.info(dev, log);
					break;
				default:
					log.info("\n\nAté mais!\n\n");
					continuar = false;
			}
		}
		scan.close();
	}
}
