class A implements B { //A
}
interface B { //B
}
interface Z {
}
class C extends A { //C
}
class D implements B, implements Z { //D
}