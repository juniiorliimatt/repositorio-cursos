package logica;

public class Questao03 {
    public static void main(String[] args) {
        int[] V = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int K = 9;
        System.out.println(busca(V, K));
    }

    public static int busca(int V[], int K) {
        int C, F, M = 0;

        F = 9;
        C = 1;

        while ((V[M] != K) || (F > C)) {

            M = (C + F) / 2;

            if (K < V[M]) {
                F = M - 1;
            } else {
                C = M + 1;
            }
        }

        if (V[M] != K) {
            return 0;
        } else {
            return M;
        }
    }
}
