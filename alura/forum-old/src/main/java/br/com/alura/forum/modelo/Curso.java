package br.com.alura.forum.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Curso{

  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;
  private String nome;
  private String categoria;

  public Curso(){
  }

  public Curso(String nome, String categoria){
    this.nome=nome;
    this.categoria=categoria;
  }

  public Long getId(){
    return id;
  }

  public String getNome(){
    return nome;
  }

  public String getCategoria(){
    return categoria;
  }

  @Override
  public boolean equals(Object o){
    if(this==o)
      return true;
    if(o==null || getClass()!=o.getClass())
      return false;
    Curso curso=(Curso) o;
    return Objects.equals(id, curso.id);
  }

  @Override
  public int hashCode(){
    return Objects.hash(id);
  }
}
