package valida.cpf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class Calcula {
	final Logger log = Logger.getLogger(Calcula.class.getName());

	private Calcula() {
		throw new IllegalStateException("Utility class");
	}

	public static List<Integer> digitoVerificadorUm(List<Integer> numerosCpf) {
		List<Integer> calculoUm = new ArrayList<>();
		List<Integer> dv1 = Arrays.asList(10, 9, 8, 7, 6, 5, 4, 3, 2);
		for (var i = 0; i < numerosCpf.size(); i++) {
			calculoUm.add(dv1.get(i) * numerosCpf.get(i));
		}
		return calculoUm;
	}

	public static List<Integer> digitoVerificadorDois(List<Integer> numerosCpf, Integer digitoUm) {
		List<Integer> calculoDois = new ArrayList<>();
		List<Integer> dv2 = Arrays.asList(11, 10, 9, 8, 7, 6, 5, 4, 3, 2);
		numerosCpf.add(digitoUm);
		for (var i = 0; i < numerosCpf.size(); i++) {
			calculoDois.add(dv2.get(i) * numerosCpf.get(i));
		}
		return calculoDois;
	}

	public static Integer soma(List<Integer> list) {
		Integer soma = 0;
		for (var i = 0; i < list.size(); i++) {
			soma += list.get(i);
		}
		return soma;
	}
}
