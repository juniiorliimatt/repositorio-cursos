package di;

import model.Cliente;
import notificacao.Notificador;
import notificacao.NotificadorSMS;
import service.AtivacaoClienteService;

public class Main {
  public static void main(String[] args) {
    var joao = new Cliente("Joao", "joao@email.com", "12345677");
    var maria = new Cliente("Maria", "maria@email.com", "12345677");

    Notificador notificador = new NotificadorSMS();

    var  ativacaoClienteService = new AtivacaoClienteService(notificador);

    ativacaoClienteService.ativar(joao);
    ativacaoClienteService.ativar(maria);
  }
}
