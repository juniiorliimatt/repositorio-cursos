package br.com.alura.loja.dao;

import br.com.alura.loja.modelo.Produto;

import javax.persistence.EntityManager;
import java.util.List;

public class ProdutoDAO{
  private final EntityManager em;
  
  public ProdutoDAO(EntityManager em){
    this.em=em;
  }
  
  public void cadastrar(Produto produto){
    this.em.persist(produto);
  }
  
  public void atualizar(Produto produto){
  
  }
  
  public void deletar(Produto produto){
  
  }
  
  public List<Produto> listarProdutos(){
    return null;
  }
  
}
