import br.com.alura.leilao.dao.LeilaoDao;
import br.com.alura.leilao.model.Leilao;
import br.com.alura.leilao.model.Usuario;
import br.com.alura.leilao.util.JPAUtil;
import br.com.alura.leilao.util.builder.LeilaoBuilder;
import br.com.alura.leilao.util.builder.UsuarioBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;

class LeilaoDaoTest{

  private EntityManager em;
  private LeilaoDao dao;

  @BeforeEach
  public void beforeEach(){
    this.em=JPAUtil.getEntityManager();
    this.dao=new LeilaoDao(em);
    em.getTransaction().begin();
  }

  @AfterEach
  public void afterEach(){
    em.getTransaction().rollback();
  }

  @Test
  void deveriaCriarUmLeilao(){
    Usuario usuario=new UsuarioBuilder().comNome("Fulano").comEmail("fulano@gmail.com").comSenha("1234567890").criar();
    em.persist(usuario);

    Leilao leilao=
        new LeilaoBuilder().comNome("Mochila").comValorInicial("70.00").comData(LocalDate.now()).comUsuario(usuario).criar();
    leilao = dao.salvar(leilao);

    Assertions.assertNotNull(dao.buscarPorId(leilao.getId()));
  }

  @Test
  void deveriaAtualizarUmLeilao(){
    Usuario usuario=new UsuarioBuilder().comNome("Fulano").comEmail("fulano@gmail.com").comSenha("1234567890").criar();
    em.persist(usuario);

    Leilao leilao=
        new LeilaoBuilder().comNome("Mochila").comValorInicial("70.00").comData(LocalDate.now()).comUsuario(usuario).criar();

    leilao = dao.salvar(leilao);

    leilao.setNome("Celular");
    leilao.setValorInicial(new BigDecimal("400.00"));
    leilao=dao.salvar(leilao);

    Assertions.assertEquals("Celular", leilao.getNome());
    Assertions.assertEquals(new BigDecimal("400.00"), leilao.getValorInicial());
  }
}
