package br.com.alura.spring.data.service;

import br.com.alura.spring.data.orm.UnidadeDeTrabalho;
import br.com.alura.spring.data.repository.UnidadeDeTrabalhoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Scanner;

@Service
public class CrudUnidadeDeTrabalhoService{
  private final UnidadeDeTrabalhoRepository repository;
  
  @Autowired
  public CrudUnidadeDeTrabalhoService(UnidadeDeTrabalhoRepository repository){
    this.repository=repository;
  }
  
  public void salvar(Scanner scanner){
    System.out.print("Descrição: ");
    scanner.next();
    String descricao=scanner.nextLine();
    System.out.print("Endereco: ");
    String endereco=scanner.nextLine();
    repository.save(new UnidadeDeTrabalho(descricao,endereco));
    System.out.println("Unidade de trabalho salva!");
  }
  
  public void atualizar(Scanner scanner){
    System.out.print("Qual o Id da unidade que deseja atualizar: ");
    Integer id = scanner.nextInt();
    System.out.print("Qual a nova descrição: ");
    String descricao = scanner.nextLine();
    System.out.print("Qual o novo endereço? ");
    String endereco = scanner.nextLine();
    repository.save(new UnidadeDeTrabalho(id, descricao, endereco));
    System.out.println();
  }
  
  public void listarTodos(Scanner scanner){
  }
  
  public void deletar(Scanner scanner){
  }
  
  public void deletarTodos(Scanner scanner){
  }
}
