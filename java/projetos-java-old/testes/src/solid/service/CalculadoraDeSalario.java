package solid.service;

import solid.entities.Funcionario;
import solid.enums.Cargo;

public class CalculadoraDeSalario{
  public double calcula(Funcionario funcionario){
    if(Cargo.DESENVOLVEDOR.equals(funcionario.getCargo())){
      return new DezOuVintePorCento().calcula(funcionario);
    }

    if(Cargo.DBA.equals(funcionario.getCargo()) || Cargo.TESTER.equals(funcionario.getCargo())){
      return new QuinzeOuVintePorCento().calcula(funcionario);
    }
    throw new RuntimeException("Funcionário inválido");
  }
}
