package br.com.alura.loja.dao;

import br.com.alura.loja.modelo.Categoria;

import javax.persistence.EntityManager;
import java.util.List;

public class CategoriaDAO{
  private final EntityManager em;
  
  public CategoriaDAO(EntityManager em){
    this.em=em;
  }
  
  public void cadastrar(Categoria categoria){
    this.em.persist(categoria);
  }
  
  public void atualizar(Categoria categoria){
    this.em.merge(categoria);
  }
  
  public void deletar(Categoria categoria){
    categoria=em.merge(categoria);
    this.em.remove(categoria);
  }
  
  public Categoria buscarPorId(Long id){
    return em.find(Categoria.class,id);
  }
  
  public List<Categoria> buscarPorNome(String nome){
    String jpql="select c from Categoria c where c.nome = :nome";
    return em.createQuery(jpql,Categoria.class).setParameter("nome",nome).getResultList();
  }
  
  public List<Categoria> buscarTodos(){
    String jpql="SELECT C FROM Categoria C";
    return em.createQuery(jpql,Categoria.class).getResultList();
  }
}
