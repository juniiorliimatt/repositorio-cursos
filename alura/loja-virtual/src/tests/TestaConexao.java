package tests;

import java.sql.Connection;
import java.sql.SQLException;

public class TestaConexao{
  public static void main(String[] args) throws SQLException{
    ConnectionFactory getCon=new ConnectionFactory();
    Connection con=getCon.getConnection();
    
    System.out.println("Conexão fechada");
    con.close();
  }
}
