package br.com.alura.loja.app;

import br.com.alura.loja.desconto.CalculadoraDeDesconto;
import br.com.alura.loja.orcamento.ItemOrcamento;
import br.com.alura.loja.orcamento.Orcamento;

import java.math.BigDecimal;

public class TesteDescontos{
  public static void main(String[] args){
    Orcamento primeiro=new Orcamento();
    primeiro.adicionarItem(new ItemOrcamento((new BigDecimal("200"))));

    Orcamento segundo=new Orcamento();
    segundo.adicionarItem(new ItemOrcamento((new BigDecimal("400"))));

    Orcamento terceiro=new Orcamento();
    terceiro.adicionarItem(new ItemOrcamento((new BigDecimal("800"))));

    CalculadoraDeDesconto calculadora=new CalculadoraDeDesconto();

    System.out.println(calculadora.calcular(primeiro));
    System.out.println(calculadora.calcular(segundo));
    System.out.println(calculadora.calcular(terceiro));

  }
}
