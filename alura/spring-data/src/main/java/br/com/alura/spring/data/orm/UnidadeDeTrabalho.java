package br.com.alura.spring.data.orm;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="unidades_de_trabalho")
public class UnidadeDeTrabalho{
  
  @Id
  @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="UNIDADE_DE_TRABALHO_SEQUENCE")
  @SequenceGenerator(sequenceName="unidade_de_trabalho_seq", allocationSize=1, name="UNIDADE_DE_TRABALHO_SEQUENCE")
  private Integer id;
  private String descricao;
  private String endereco;
  
  @ManyToMany(mappedBy="unidadeDeTrabalho", fetch=FetchType.EAGER)
  private List<Funcionario> funcionarios=new ArrayList<>();
  
  public UnidadeDeTrabalho(){
  }
  
  public UnidadeDeTrabalho(String descricao,String endereco){
    this.descricao=descricao;
    this.endereco=endereco;
  }
  
  public UnidadeDeTrabalho(Integer id,String descricao,String endereco){
    this.id=id;
    this.descricao=descricao;
    this.endereco=endereco;
  }
  
  public Integer getId(){
    return id;
  }
  
  public String getDescricao(){
    return descricao;
  }
  
  public String getEndereco(){
    return endereco;
  }
  
  @Override
  public String toString(){
    return "UnidadeDeTrabalho{"+"id="+id+", descricao='"+descricao+'\''+", endereco='"+endereco+'\''+'}';
  }
}
