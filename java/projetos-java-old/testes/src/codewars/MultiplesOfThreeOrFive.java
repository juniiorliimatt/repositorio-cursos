package codewars;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class MultiplesOfThreeOrFive {
    public static void main(String[] args) {
        var log = Logger.getLogger(MultiplesOfThreeOrFive.class.getPackageName());
        var result = "" + solution(19);
        log.info(result);

    }

    public static int solution(int number) {
        List<Integer> numbers = new ArrayList<>();
        List<Integer> multiples = new ArrayList<>();

        var soma = 0;

        for (var i = 0; i < number; i++) {
            numbers.add(i);
        }

        for (Integer n : numbers) {
            if (n % 3 == 0 || n % 5 == 0) {
                multiples.add(n);
            }
        }

        for (Integer n : multiples) {
            soma += n;
        }
        return soma;
    }
}
