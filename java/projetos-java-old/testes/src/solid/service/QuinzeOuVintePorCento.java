package solid.service;

import solid.entities.Funcionario;

public class QuinzeOuVintePorCento implements RegraDeCalculo{
  @Override
  public double calcula(Funcionario funcionario){
    if(funcionario.getSalarioBase()>2000){
      return funcionario.getSalarioBase()*0.75;
    }else{
      return funcionario.getSalarioBase()*0.85;
    }
  }
}
