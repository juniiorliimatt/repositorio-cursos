package domain;

import java.io.Serializable;
import java.util.Objects;

public class Person implements Serializable{
  private Integer id;
  private String nome;
  private String email;

  public Person(){
  }

  public Person(Integer id, String nome, String email){
    this.id = id;
    this.nome = nome;
    this.email = email;
  }

  public Integer getId(){
    return id;
  }

  public void setId(Integer id){
    this.id = id;
  }

  public String getNome(){
    return nome;
  }

  public void setNome(String nome){
    this.nome = nome;
  }

  public String getEmail(){
    return email;
  }

  public void setEmail(String email){
    this.email = email;
  }

  @Override
  public boolean equals(Object o){
    if(this==o) return true;
    if(o==null||getClass()!=o.getClass()) return false;
    Person person = (Person)o;
    return Objects.equals(getId(), person.getId());
  }

  @Override
  public int hashCode(){
    return Objects.hash(getId());
  }

  @Override
  public String toString(){
    return "Person{"+
        "id="+id+
        ", nome='"+nome+'\''+
        ", email='"+email+'\''+
        '}';
  }
}
