package thirtydaysofcode;

import java.util.Scanner;

public class DayFour{
  public static void main(String[] args){
    var sc = new Scanner(System.in);
    var qtd = sc.nextInt();

    for(var i = 0;i<qtd;i++){
      var age = sc.nextInt();
      var person = new Person(age);
      person.amIOld();
      person.yearPasses(3);
    }

    sc.close();
  }
}

class Person{
  private int age;

  public Person(int age){
    if(age<=0){
      this.age = 0;
      System.out.println("Age is not valid, setting age to 0.");
    }
    this.age = age;
  }

  public void yearPasses(int age){
    this.age += age;
  }

  public void amIOld(){
    if(age<13){
      System.out.println("You are young.");
    }else if(age<18){
      System.out.println("You are a teenager.");
    }else{
      System.out.println("You are old.");
    }
  }

  public int getAge(){
    return this.age;
  }

  public void setAge(int age){
    this.age = age;
  }
}
