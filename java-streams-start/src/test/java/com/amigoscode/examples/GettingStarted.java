package com.amigoscode.examples;

import com.amigoscode.beans.Person;
import com.amigoscode.mockdata.MockData;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GettingStarted{
  // 1. Find people aged less or equal 18
  // 2. Then change implementation to find first 10 people

  @Test
  public void imperativeApproach() throws IOException{

    List<Person> peoples=MockData.getPeople();
    List<Person> peopleLessOrEquals18=new ArrayList<>();

    int limit=10;
    int counter=0;

    for(Person person: peoples){
      if(person.getAge()<=18){
        peopleLessOrEquals18.add(person);
        counter++;
        if(counter==limit){
          break;
        }
      }
    }
    for(Person person: peopleLessOrEquals18){
      System.out.println(person);
    }
  }

  // O método acima, implementado com programação imperativa, pode ser reproduzido a baixo com uma abordagem declarativa.
  @Test
  public void declarativeApproachUsingStreams() throws Exception{
    List<Person> peoples=MockData.getPeople();

    List<Person> peopleLessOrEquals18First10=peoples
        .stream()
        .filter(people->people.getAge()<=18)
        .limit(10)
        .collect(Collectors.toList());
    peopleLessOrEquals18First10.forEach(System.out::println);
  }
}
