package br.com.alura.leilao.login;

import br.com.alura.leilao.leilao.LeilaoPage;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginPage{

  private static final String URL_LOGIN="http://localhost:8080/login";
  private final WebDriver browser;

  public LoginPage(){
    System.setProperty("webdriver.chrome.driver", "C:\\dev\\alura\\libs\\chromedriver.exe");
    this.browser=new ChromeDriver();
    this.browser.navigate().to(URL_LOGIN);
  }

  public void preencheFormularioDeLogin(String username, String password){
    this.browser.findElement(By.id("username")).sendKeys(username);
    this.browser.findElement(By.id("password")).sendKeys(password);
  }

  public LeilaoPage efetuarLogin(){
    this.browser.findElement(By.id("login-form")).submit();
    return new LeilaoPage(browser);
  }

  public boolean isPaginaLogin(){
    return this.browser.getCurrentUrl().equals(URL_LOGIN);
  }

  public boolean isPaginaLoginComDadosInvalidos(){
    return this.browser.getCurrentUrl().equals(URL_LOGIN+"?error");
  }

  public String getNomeUsuarioLogado(){
    try{
      return this.browser.findElement(By.id("usuario-logado")).getText();
    }catch(NoSuchElementException e){
      return null;
    }
  }

  public void navegaParaPaginaDeLances(){
    this.browser.navigate().to("http://localhost:8080/leiloes/2");
  }

  public boolean contemTexto(String texto){
    return this.browser.getPageSource().contains(texto);
  }

  public void fecharBrowser(){
    this.browser.quit();
  }
}
