package model.dao.impl;

import db.DB;
import db.DbException;
import model.dao.DepartmentDao;
import model.entities.Department;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DepartmentDaoJDBC implements DepartmentDao{
  private final Connection connection;

  public DepartmentDaoJDBC(Connection connection){
    this.connection = connection;
  }

  @Override
  public void insert(Department department){
    PreparedStatement st = null;
    try{
      st = connection.prepareStatement("INSERT INTO department (Name) VALUES (?)");
      st.setString(1, department.getName());
      st.executeUpdate();

    }catch(SQLException e){
      throw new DbException(e.getMessage());
    }finally{
      closeConnection(st);
    }
  }

  @Override
  public void update(Department department){
    PreparedStatement st = null;
    try{
      st = connection.prepareStatement("UPDATE department SET Name = ? WHERE Id = ?");
      st.setString(1, department.getName());
      st.setInt(2, department.getId());

      st.executeUpdate();
    }catch(SQLException e){
      throw new DbException(e.getMessage());
    }finally{
      closeConnection(st);
    }
  }

  @Override
  public void deleteById(Integer id){
    PreparedStatement st = null;
    try{
      st = connection.prepareStatement("DELETE FROM department WHERE Id = ?");
      st.setInt(1, id);
      st.executeUpdate();
    }catch(SQLException e){
      throw new DbException(e.getMessage());
    }finally{
      closeConnection(st);
    }
  }

  @Override
  public Department findById(Integer id){
    PreparedStatement st = null;
    ResultSet rs = null;
    try{
      st = connection.prepareStatement("SELECT Id, Name FROM Department WHERE Id = ?");
      st.setInt(1, id);
      rs = st.executeQuery();

      if(rs.next()){
        return new Department(rs.getInt("Id"), rs.getString("Name"));
      }
      return null;
    }catch(SQLException e){
      throw new DbException(e.getMessage());
    }finally{
      closeConnection(st);
    }
  }

  @Override
  public List<Department> findAll(){
    PreparedStatement st = null;
    ResultSet rs = null;
    try{
      st = connection.prepareStatement("SELECT Id, Name FROM Department");
      rs = st.executeQuery();

      List<Department> departments = new ArrayList<>();
      while(rs.next()){
        departments.add(new Department(rs.getInt("Id"), rs.getString("Name")));
      }
      return departments;
    }catch(SQLException e){
      throw new DbException(e.getMessage());
    }finally{
      closeConnections(rs, st);
    }
  }

  private void closeConnection(Statement st){
    DB.closeStatement(st);
  }

  private void closeConnections(ResultSet rs, Statement st){
    DB.closeResultSet(rs);
    DB.closeStatement(st);
  }
}
