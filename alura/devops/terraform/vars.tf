variable "amis" {
  default = {
    "us-east-1"="ami-0e472ba40eb589f49"
    "us-east-2"="ami-002068ed284fb165b"
  }
}

variable "cdirs_acesso_remoto" {
  default = ["45.179.224.104/32"]
}

variable "key_name" {
  default = "terraform-aws"
}