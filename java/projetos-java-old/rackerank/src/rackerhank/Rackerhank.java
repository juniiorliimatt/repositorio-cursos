package rackerhank;

import java.util.Scanner;

public class Rackerhank {
  public static void main(String[] args) {
    var sc = new Scanner(System.in);
    var x = sc.nextShort();
    var l = "* long";
    var i = "* int";

    for (var y = 0; y < x; y++) {
      try {
        var number = sc.nextLong();
        System.out.println(number + " can be fitted in:");
        if (number >= Byte.MIN_VALUE && number <= Byte.MAX_VALUE) {
          System.out.println("* byte");
          System.out.println("* short");
          System.out.println(i);
          System.out.println(l);
        } else if (number >= Short.MIN_VALUE && number <= Short.MAX_VALUE) {
          System.out.println("* short");
          System.out.println(i);
          System.out.println(l);
        } else if (number >= Integer.MIN_VALUE && number <= Integer.MAX_VALUE) {
          System.out.println(i);
          System.out.println(l);
        } else {
          System.out.println(l);
        }
      } catch (Exception e) {
        System.out.println(sc.next() + " can't be fitted anywhere.");
      }
    }

    sc.close();
  }
}
