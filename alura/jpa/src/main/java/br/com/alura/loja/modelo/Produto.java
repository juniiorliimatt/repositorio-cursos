package br.com.alura.loja.modelo;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name="produtos")
public class Produto{
  
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;
  
  private String nome;
  private String descricao;
  private BigDecimal preco;
  private LocalDate cadastrado=LocalDate.now();
  
  @ManyToOne(cascade=CascadeType.ALL)
  private Categoria categoria;
  
  public Produto(){
  }
  
  public Produto(String nome,String descricao,BigDecimal preco,Categoria categoria){
    this.nome=nome;
    this.descricao=descricao;
    this.preco=preco;
    this.categoria=categoria;
  }
  
  public Produto(String nome,String descricao,BigDecimal preco,LocalDate cadastrado,Categoria categoria){
    this.nome=nome;
    this.descricao=descricao;
    this.preco=preco;
    this.cadastrado=cadastrado;
    this.categoria=categoria;
  }
  
  public Produto(Long id){
    this.id=id;
  }
  
  public String getNome(){
    return nome;
  }
  
  public void setNome(String nome){
    this.nome=nome;
  }
  
  public String getDescricao(){
    return descricao;
  }
  
  public void setDescricao(String descricao){
    this.descricao=descricao;
  }
  
  public BigDecimal getPreco(){
    return preco;
  }
  
  public void setPreco(BigDecimal preco){
    this.preco=preco;
  }
  
  public LocalDate getCadastrado(){
    return cadastrado;
  }
  
  public void setCadastrado(LocalDate cadastrado){
    this.cadastrado=cadastrado;
  }
  
  public Categoria getCategoria(){
    return categoria;
  }
  
  public void setCategoria(Categoria categoria){
    this.categoria=categoria;
  }
}
