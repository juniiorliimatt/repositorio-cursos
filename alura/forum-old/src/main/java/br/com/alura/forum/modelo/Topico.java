package br.com.alura.forum.modelo;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Topico{

  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;
  private String titulo;
  private String mensagem;
  private LocalDateTime dataCriacao=LocalDateTime.now();

  @Enumerated(EnumType.STRING)
  private StatusTopico status=StatusTopico.NAO_RESPONDIDO;

  @ManyToOne
  private Usuario autor;

  @ManyToOne
  private Curso curso;

  @OneToMany(mappedBy="topico")
  private List<Resposta> respostas=new ArrayList<>();

  public Topico(){
  }

  public Topico(String titulo, String mensagem, Curso curso){
    this.titulo=titulo;
    this.mensagem=mensagem;
    this.curso=curso;
  }

  public Topico(Long id, String titulo, String mensagem, LocalDateTime dataCriacao, List<Resposta> respostas, StatusTopico status, Usuario autor, Curso curso){
    this.id=id;
    this.titulo=titulo;
    this.mensagem=mensagem;
    this.dataCriacao=dataCriacao;
    this.respostas=respostas;
    this.status=status;
    this.autor=autor;
    this.curso=curso;
  }

  public Long getId(){
    return id;
  }

  public String getTitulo(){
    return titulo;
  }

  public String getMensagem(){
    return mensagem;
  }

  public LocalDateTime getDataCriacao(){
    return dataCriacao;
  }

  public StatusTopico getStatus(){
    return status;
  }

  public Usuario getAutor(){
    return autor;
  }

  public Curso getCurso(){
    return curso;
  }

  public List<Resposta> getRespostas(){
    return respostas;
  }

  @Override
  public boolean equals(Object o){
    if(this==o)
      return true;
    if(o==null || getClass()!=o.getClass())
      return false;
    Topico topico=(Topico) o;
    return Objects.equals(id, topico.id);
  }

  @Override
  public int hashCode(){
    return Objects.hash(id);
  }
}
