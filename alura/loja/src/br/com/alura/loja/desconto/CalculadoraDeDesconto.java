package br.com.alura.loja.desconto;

import br.com.alura.loja.orcamento.Orcamento;

import java.math.BigDecimal;

/**
 * Chain of Responsibility
 * @author Junior
 */
public class CalculadoraDeDesconto{
  public BigDecimal calcular(Orcamento orcamento){
    Desconto cadeiaDeDescontos = new DescontoParaOrcamentoComMaisDeCincoItens(
        new DescontoParaOrcamentoComValorMaiorQueQuinhentos(
            new SemDesconto())
        );
    return cadeiaDeDescontos.calcular(orcamento);
  }
}
