package solid.service;

import solid.entities.Funcionario;

public interface RegraDeCalculo{
  double calcula(Funcionario funcionario);
}
