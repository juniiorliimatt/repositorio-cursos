package br.com.alura.loja.app;

import br.com.alura.loja.dao.CategoriaDAO;
import br.com.alura.loja.dao.ProdutoDAO;
import br.com.alura.loja.modelo.Categoria;
import br.com.alura.loja.modelo.Produto;
import br.com.alura.loja.util.JPAUtil;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

public class CadastroDeProduto{
  public static void main(String[] args){
    EntityManager em=JPAUtil.getEntityManager();
    CategoriaDAO categoriaDAO=new CategoriaDAO(em);
    
    
    List<Categoria> todos=categoriaDAO.buscarPorNome("CELULAR");
    todos.forEach(c ->System.out.println(c.getNome()));
  }
  
  public static void cadastrarProduto(){
    Categoria categoria=new Categoria("CELULAR");
    Produto produto=new Produto("Xiaomi","Muito Legal",new BigDecimal("800.00"),categoria);
    
    EntityManager em=JPAUtil.getEntityManager();
    ProdutoDAO produtoDAO=new ProdutoDAO(em);
    CategoriaDAO categoriaDAO=new CategoriaDAO(em);
    
    em.getTransaction().begin();
    categoriaDAO.cadastrar(categoria);
    produtoDAO.cadastrar(produto);
    em.getTransaction().commit();
    em.close();
  }
}
