package br.com.alura.forum.controller.form;

import br.com.alura.forum.modelo.Curso;
import br.com.alura.forum.modelo.Topico;
import br.com.alura.forum.repository.CursoRepository;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


public class TopicoForm{

  @NotNull @NotEmpty @Length(min=5)
  private String titulo;

  @NotNull @NotEmpty @Length(min=10)
  private String mensagem;

  @NotNull @NotEmpty @Length(min=5)
  private String nomeCurso;

  public TopicoForm(){
  }

  public TopicoForm(String titulo, String mensagem, String nomeCurso){
    this.titulo=titulo;
    this.mensagem=mensagem;
    this.nomeCurso=nomeCurso;
  }

  public String getTitulo(){
    return titulo;
  }

  public String getMensagem(){
    return mensagem;
  }

  public String getNomeCurso(){
    return nomeCurso;
  }

  public Topico converter(CursoRepository repository){
    Curso curso=repository.findByNome(nomeCurso);
    return new Topico(titulo, mensagem, curso);
  }
}
