package br.com.alura.loja.app;

import br.com.alura.loja.http.HttpCliente;
import br.com.alura.loja.orcamento.Orcamento;
import br.com.alura.loja.orcamento.RegistroDeOrcamento;

public class TesteAdapter{
  public static void main(String[] args){
    Orcamento orcamento=new Orcamento();
    orcamento.aprovar();
    orcamento.finalizar();

    RegistroDeOrcamento registro=new RegistroDeOrcamento(new HttpCliente());
    registro.registrar(orcamento);
  }
}
