package br.com.alura.leilao.login;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LoginTeste{

  private LoginPage loginPage;

  @BeforeEach
  void beforeEach(){
    this.loginPage=new LoginPage();
  }

  @AfterEach
  void afterEach(){
    this.loginPage.fecharBrowser();
  }

  @Test
  void deveriaEfetuarLoginComDadosValidos(){
    loginPage.preencheFormularioDeLogin("fulano", "pass");
    loginPage.efetuarLogin();

    Assertions.assertFalse(loginPage.isPaginaLogin());
    Assertions.assertEquals("fulano", loginPage.getNomeUsuarioLogado());
  }

  @Test
  void naoDeveriaEfetuarLoginComDadosInvalidos(){
    loginPage.preencheFormularioDeLogin("cicrano", "pass");
    loginPage.efetuarLogin();

    Assertions.assertTrue(loginPage.isPaginaLoginComDadosInvalidos());
    Assertions.assertNull(loginPage.getNomeUsuarioLogado());
    Assertions.assertTrue(loginPage.contemTexto("Usuário e senha inválidos."));
  }

  @Test
  void naoDeveriaAcessarPaginaRestritaSemEstarLogado(){
    loginPage.preencheFormularioDeLogin("cicrano", "pass");
    loginPage.navegaParaPaginaDeLances();

    Assertions.assertTrue(loginPage.isPaginaLogin());
    Assertions.assertFalse(loginPage.contemTexto("Dados do Leilão."));
  }
}
