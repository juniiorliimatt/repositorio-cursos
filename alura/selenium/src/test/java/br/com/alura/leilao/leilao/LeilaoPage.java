package br.com.alura.leilao.leilao;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LeilaoPage{

  private final static String URL_CADASTRO_LEILAO="http://localhost:8080/leiloes/new";
  private final static String URL_LEILOES="http://localhost:8080/leiloes";
  private WebDriver browser;

  public LeilaoPage(WebDriver browser){
    System.setProperty("webdriver.chrome.driver", "C:\\dev\\alura\\libs\\chromedriver.exe");
    this.browser=browser;
  }

  public void fecharBrowser(){
    this.browser.quit();
  }

  public CadastroLeilaoPage carregarFormulario(){
    this.browser.navigate().to(URL_CADASTRO_LEILAO);
    return new CadastroLeilaoPage(browser);
  }

  public boolean isLeilaoCadastrado(String nome, String valor, String hoje){
    WebElement linhaDaTabela=this.browser.findElement(By.cssSelector("#tabela-leiloes tbody tr:last-child"));
    WebElement colunaNome=linhaDaTabela.findElement(By.cssSelector("td:nth-child(1)"));
    WebElement colunaDataAbertura=linhaDaTabela.findElement(By.cssSelector("td:nth-child(2)"));
    WebElement colunaValorInicial=linhaDaTabela.findElement(By.cssSelector("td:nth-child(3)"));

    return colunaNome.getText().equals(nome) && colunaDataAbertura.getText().equals(hoje) && colunaValorInicial.getText().equals(valor);
  }

  public boolean isPaginaAtual(){
    return browser.getCurrentUrl().contentEquals(URL_LEILOES);
  }
}
