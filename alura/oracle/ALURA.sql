SELECT * FROM ITENS_NOTAS_FISCAIS ORDER BY NUMERO;
SELECT EMBALAGEM FROM TABELA_DE_PRODUTOS;
SELECT DISTINCT EMBALAGEM FROM TABELA_DE_PRODUTOS;
SELECT DISTINCT SABOR FROM TABELA_DE_PRODUTOS;
SELECT DISTINCT EMBALAGEM, SABOR  FROM TABELA_DE_PRODUTOS ORDER BY EMBALAGEM;
SELECT ROWNUM, CODIGO_DO_PRODUTO, NOME_DO_PRODUTO FROM TABELA_DE_PRODUTOS;
SELECT ROWNUM, CODIGO_DO_PRODUTO, NOME_DO_PRODUTO FROM TABELA_DE_PRODUTOS WHERE ROWNUM <= 5;
SELECT * FROM TABELA_DE_PRODUTOS ORDER BY TAMANHO DESC, PRECO_DE_LISTA ASC;
SELECT * FROM TABELA_DE_PRODUTOS WHERE NOME_DO_PRODUTO = 'Linha Refrescante - 1 Litro - Morango/Limao';
SELECT CODIGO_DO_PRODUTO, SUM(PRECO) FROM ITENS_NOTAS_FISCAIS  GROUP BY CODIGO_DO_PRODUTO ORDER BY SUM(PRECO) DESC;

SELECT * FROM TABELA_DE_PRODUTOS WHERE CODIGO_DO_PRODUTO = '1101035';

SELECT * FROM ITENS_NOTAS_FISCAIS;
SELECT COUNT(*) AS QUANTIDADE_DE_ITENS, MAX(QUANTIDADE) AS MAIOR_QUANTIDADE FROM ITENS_NOTAS_FISCAIS WHERE CODIGO_DO_PRODUTO = '1101035' GROUP BY QUANTIDADE ORDER BY QUANTIDADE DESC;

SELECT COUNT(*) FROM ITENS_NOTAS_FISCAIS WHERE CODIGO_DO_PRODUTO = '1101035' AND  QUANTIDADE = 99;

SELECT AVG( QUANTIDADE),
	(CASE
		WHEN QUANTIDADE >10 AND QUANTIDADE < 30 THEN 'VENDAS BAIXAS'
		WHEN QUANTIDADE > 30 AND QUANTIDADE < 60 THEN 'VENDAS M�DIAS'
		WHEN QUANTIDADE > 60 AND QUANTIDADE <100 THEN 'VENDAS ALTAS'
		ELSE 'VENDAS RUINS'
	END) AS VENDAS
FROM ITENS_NOTAS_FISCAIS 
GROUP BY QUANTIDADE,
CASE
		WHEN QUANTIDADE >10 AND QUANTIDADE < 30 THEN 'VENDAS BAIXAS'
		WHEN QUANTIDADE > 30 AND QUANTIDADE < 60 THEN 'VENDAS M�DIAS'
		WHEN QUANTIDADE > 60 AND QUANTIDADE <100 THEN 'VENDAS ALTAS'
		ELSE 'VENDAS RUINS'
END
ORDER BY VENDAS;



SELECT * FROM TABELA_DE_CLIENTES;
SELECT * FROM TABELA_DE_CLIENTES WHERE CIDADE = 'Rio de Janeiro';
SELECT DISTINCT NOME, CIDADE FROM TABELA_DE_CLIENTES ORDER BY CIDADE;
SELECT DISTINCT BAIRRO FROM TABELA_DE_CLIENTES WHERE CIDADE = 'Rio de Janeiro'  ORDER BY BAIRRO;
SELECT DISTINCT COUNT(BAIRRO) FROM TABELA_DE_CLIENTES ORDER BY BAIRRO;

SELECT NOME,(
	CASE
		WHEN DATA_DE_NASCIMENTO < TO_DATE('31/12/1970', 'DD/MM/YYYY') THEN 'VELHOS'
		WHEN DATA_DE_NASCIMENTO > TO_DATE('31/12/1989', 'DD/MM/YYYY')  AND  DATA_DE_NASCIMENTO < TO_DATE('31/12/1995', 'DD/MM/YYYY') THEN 'JOVENS'
		WHEN DATA_DE_NASCIMENTO > TO_DATE('31/12/1995', 'DD/MM/YYYY') THEN 'CRIAN�AS'
	ELSE 'OUTRAS DATAS'
	END
) AS MATURIDADE FROM TABELA_DE_CLIENTES
ORDER BY 
CASE
		WHEN DATA_DE_NASCIMENTO < TO_DATE('31/12/1970', 'DD/MM/YYYY') THEN 'VELHOS'
		WHEN DATA_DE_NASCIMENTO > TO_DATE('31/12/1989', 'DD/MM/YYYY')  AND  DATA_DE_NASCIMENTO < TO_DATE('31/12/1995', 'DD/MM/YYYY') THEN 'JOVENS'
		WHEN DATA_DE_NASCIMENTO > TO_DATE('31/12/1995', 'DD/MM/YYYY') THEN 'CRIAN�AS'
	ELSE 'OUTRAS DATAS'
END;

SELECT * FROM NOTAS_FISCAIS;
SELECT * FROM TABELA_DE_CLIENTES;

SELECT CPF, COUNT(*) FROM NOTAS_FISCAIS
WHERE TO_CHAR(DATA_VENDA, 'YYYY') = '2016'
GROUP BY CPF
HAVING COUNT(*) > 2000;

SELECT * FROM NOTAS_FISCAIS WHERE TO_CHAR(DATA_VENDA, 'DD/MM/YYYY') = '01/01/2017' AND ROWNUM <=10 ;
SELECT * FROM NOTAS_FISCAIS;


SELECT NF.MATRICULA, TV.NOME, COUNT(*) FROM NOTAS_FISCAIS NF
INNER JOIN TABELA_DE_VENDEDORES TV
ON NF.MATRICULA = TV.MATRICULA
GROUP BY NF.MATRICULA, TV.NOME;


SELECT * FROM ITENS_NOTAS_FISCAIS;
SELECT * FROM NOTAS_FISCAIS;

SELECT TO_CHAR(NF.DATA_VENDA,'YYYY') AS ANO, SUM(INF.QUANTIDADE * INF.PRECO) AS FATURAMENTO
FROM NOTAS_FISCAIS NF
INNER JOIN ITENS_NOTAS_FISCAIS INF
ON NF.NUMERO = INF.NUMERO
GROUP BY TO_CHAR(DATA_VENDA, 'YYYY');

SELECT * FROM TABELA_DE_CLIENTES;
SELECT DISTINCT TC.CPF AS CPF_TC, TC.NOME, NF.CPF AS CPF_NF FROM TABELA_DE_CLIENTES TC LEFT JOIN NOTAS_FISCAIS NF ON TC.CPF = NF.CPF;

SELECT Z.CPF, Z.CONTADOR FROM 
(
	SELECT CPF, COUNT(*) AS CONTADOR FROM NOTAS_FISCAIS WHERE TO_CHAR(DATA_VENDA, 'YYYY') = 2016 GROUP BY CPF
)  Z
WHERE Z.CONTADOR > 2000;

SELECT * FROM TABELA_DE_CLIENTES;

SELECT NOME, 'Endere�o: ' || ENDERECO_1 || ' ' || BAIRRO || ' ' || CIDADE || ' ' || ESTADO || ' ' || 'Cep: ' || CEP AS ENDERECO FROM TABELA_DE_CLIENTES;

SELECT MONTHS_BETWEEN (SYSDATE, TO_DATE('03/03/1990', 'DD/MM/YYYY')) * 30 * 24 * 60 AS IDADE FROM DUAL;

SELECT 'O cliente ' || TC.NOME || ' faturou ' || 
TO_CHAR(ROUND(SUM(INF.QUANTIDADE * INF.preco),2)) || ' no ano ' || TO_CHAR(DATA_VENDA, 'YYYY') AS SENTENCA 
FROM notas_fiscais NF
INNER JOIN ITENS_NOTAS_FISCAIS INF ON NF.NUMERO = INF.NUMERO
INNER JOIN TABELA_DE_CLIENTES TC ON NF.CPF = TC.CPF
WHERE TO_CHAR(DATA_VENDA, 'YYYY') = '2016'
GROUP BY TC.NOME, TO_CHAR(DATA_VENDA, 'YYYY');


select * from notas_fiscais;







