package curso;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

class Curso{
  private String nome;
  private int alunos;

  public Curso(String nome, int alunos){
    this.nome=nome;
    this.alunos=alunos;
  }

  public String getNome(){
    return this.nome;
  }

  public int getalunos(){
    return this.alunos;
  }

  @Override
  public String toString(){
    return "Curso{"+"nome='"+nome+'\''+", alunos="+alunos+'}';
  }
}

public class ExemploCurso{
  public static void main(String[] args){
    List<Curso> cursos=new ArrayList<>();
    cursos.add(new Curso("Python", 45));
    cursos.add(new Curso("Javascript", 150));
    cursos.add(new Curso("Java 8", 113));
    cursos.add(new Curso("C", 55));

    cursos.sort(Comparator.comparing(Curso::getalunos));
    cursos.forEach(c -> System.out.println(c.getNome()));

    System.out.println();

    var cursosMaiorQueTrinta = cursos.stream().filter(c -> c.getalunos() > 100);
    cursosMaiorQueTrinta.forEach(c ->System.out.println(c.getNome()));

    System.out.println();

    cursos.stream().filter(c -> c.getalunos() < 100).forEach(System.out::println);

    System.out.println();

    cursos.stream().filter(c -> c.getalunos() > 100).map(Curso::getalunos).forEach(System.out::println);
  }
}