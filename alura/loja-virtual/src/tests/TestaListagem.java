package tests;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TestaListagem{
  public static void main(String[] args) throws SQLException{
    
    ConnectionFactory getCon=new ConnectionFactory();
    Connection con=getCon.getConnection();
    
    try{
      PreparedStatement stm=con.prepareStatement("SELECT ID, NOME, DESCRICAO FROM PRODUTO");
      stm.execute();
      ResultSet resultSet=stm.getResultSet();
      while(resultSet.next()){
        Integer id=resultSet.getInt("id");
        String nome=resultSet.getString("nome");
        String descricao=resultSet.getString("descricao");
        System.out.println(id+", "+nome+", "+descricao);
      }
    }catch(SQLException e){
      throw new RuntimeException(e);
    }finally{
      con.close();
    }
    
  }
}
