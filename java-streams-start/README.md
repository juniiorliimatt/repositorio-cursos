# Java Streams API

# Descrição
A programação funcional está se tornando muito popular e se concentra em funções puras. Os aplicativos funcionais evitam o estado compartilhado e tendem a ser mais concisos e previsíveis do que aqueles que usam código orientado a objetos.

