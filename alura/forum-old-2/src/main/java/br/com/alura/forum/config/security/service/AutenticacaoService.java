package br.com.alura.forum.config.security.service;

import br.com.alura.forum.modelo.Usuario;
import br.com.alura.forum.repository.UsuarioRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AutenticacaoService implements UserDetailsService{

  private UsuarioRepository repository;

  public AutenticacaoService(UsuarioRepository repository){
    this.repository=repository;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
    Optional<Usuario> usuario=repository.findByEmail(username);
    if(usuario.isPresent()){
      return usuario.get();
    }
    throw new UsernameNotFoundException("Dados inválidos");
  }
}
