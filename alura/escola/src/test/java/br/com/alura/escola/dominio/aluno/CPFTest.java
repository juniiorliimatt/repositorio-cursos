package br.com.alura.escola.dominio.aluno;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CPFTest{
	@Test
	void naoDeveriaCriarCPFComDadosInvalido(){
		Assertions.assertThrows(IllegalArgumentException.class, ()->new CPF(null));
		Assertions.assertThrows(IllegalArgumentException.class, ()->new CPF(""));
		Assertions.assertThrows(IllegalArgumentException.class, ()->new CPF("03577488328"));
	}
}
