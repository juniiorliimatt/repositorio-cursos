package br.com.alura.spring.data.service;

import br.com.alura.spring.data.repository.FuncionarioRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Scanner;

@Service
public class CrudFuncionarioService{
  private final FuncionarioRepository repository;
  private final CrudCargoService crudCargoService;
  Scanner scanner=new Scanner(System.in);
  
  public CrudFuncionarioService(FuncionarioRepository repository,CrudCargoService crudCargoService){
    this.repository=repository;
    this.crudCargoService=crudCargoService;
  }
  
  public void salvar(){
    System.out.println("\nNovo Funcionário");
    System.out.println("Digite o nome:");
    String nome=scanner.next();
    
    System.out.println("Digite o CPF:");
    String cpf=scanner.next();
    
    System.out.println("Digite o salário:");
    BigDecimal salario=scanner.nextBigDecimal();
    
    LocalDate dataCotnratacao=LocalDate.now();
    
    System.out.println("Qual cargo?");
    crudCargoService.listarTodos();
    Integer cargoId=scanner.nextInt();
    
    System.out.println("Unidade de Trabalho");
    scanner.close();
  }
  
  public void atualizar(){
    scanner.close();
  }
  
  public void listarTodos(){
    scanner.close();
  }
  
  public void deletar(){
    scanner.close();
  }
  
  public void deletarTodos(){
    scanner.close();
  }
}
