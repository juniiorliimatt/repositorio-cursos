package br.com.alura.forum.config.security;

import br.com.alura.forum.config.security.service.AutenticacaoService;
import br.com.alura.forum.config.security.service.TokenService;
import br.com.alura.forum.repository.UsuarioRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
@Configuration
public class SecurityConfigurations extends WebSecurityConfigurerAdapter{

  private final AutenticacaoService autenticacaoService;
  private final TokenService tokenService;
  private final UsuarioRepository usuarioRepository;

  public SecurityConfigurations(AutenticacaoService autenticacaoService, TokenService tokenService,
                                UsuarioRepository usuarioRepository){
    this.autenticacaoService=autenticacaoService;
    this.tokenService=tokenService;
    this.usuarioRepository=usuarioRepository;
  }

  @Bean
  @Override
  protected AuthenticationManager authenticationManager() throws Exception{
    return super.authenticationManager();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception{
    auth
        .userDetailsService(autenticacaoService)
        .passwordEncoder(new BCryptPasswordEncoder());
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception{
    http
        .authorizeRequests()
        .antMatchers(HttpMethod.GET, "/topicos")
        .permitAll()
        .antMatchers(HttpMethod.GET, "/topicos/*")
        .permitAll()
        .antMatchers(HttpMethod.POST, "/auth")
        .permitAll()
        .antMatchers(HttpMethod.GET, "/actuator/**")
        .permitAll()
        .antMatchers(HttpMethod.DELETE, "/topicos/*")
        .hasRole("MODERADOR")
        .anyRequest()
        .authenticated()
        .and()
        .csrf()
        .disable()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .addFilterBefore(new AutenticacaoViaTokenFilter(tokenService, usuarioRepository),
                         UsernamePasswordAuthenticationFilter.class);
  }

  @Override
  public void configure(WebSecurity web) throws Exception{
    web
        .ignoring()
        .antMatchers("/**.html", "/v2/api-docs", "/webjars/**", "/configuration/**", "/swagger-resources/**");
  }

  //  public static void main(String[] args){
  //    System.out.println(new BCryptPasswordEncoder().encode("123456"));
  //  }
}
