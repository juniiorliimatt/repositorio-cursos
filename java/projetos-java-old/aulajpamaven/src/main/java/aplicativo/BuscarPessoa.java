package aplicativo;

import dominio.Pessoa;

import javax.persistence.Persistence;

public class BuscarPessoa{
  public static void main(String[] args){
    var emf = Persistence.createEntityManagerFactory("exemplo-jpa");
    var em = emf.createEntityManager();

    Pessoa p = em.find(Pessoa.class, 2);
    System.out.println(p);
  }
}
