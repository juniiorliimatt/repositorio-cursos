package solid.enums;

import solid.service.DezOuVintePorCento;
import solid.service.QuinzeOuVintePorCento;
import solid.service.RegraDeCalculo;

public enum Cargo{
  DESENVOLVEDOR(new DezOuVintePorCento()), DBA(new QuinzeOuVintePorCento()),
  TESTER(new QuinzeOuVintePorCento());

  private final RegraDeCalculo regra;

  Cargo(RegraDeCalculo regra){
    this.regra=regra;
  }

  public RegraDeCalculo getRegra(){
    return regra;
  }
}
