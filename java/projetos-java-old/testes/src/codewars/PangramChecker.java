package codewars;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PangramChecker {
    public static void main(String[] args) {
        final var log = Logger.getLogger((PangramChecker.class.getName()));
        var result = "" + new PangramChecker().check("the quick brown fox jumps over the lazy dog");
        log.log(Level.INFO, result);
    }

    public boolean check(String sentence) {
        List<Character> alphabet = Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
        List<Character> frase = new ArrayList<>();
        for (char sentenceChar : sentence.toCharArray()) {
            frase.add(sentenceChar);
        }
        return frase.containsAll(alphabet);
    }
}
