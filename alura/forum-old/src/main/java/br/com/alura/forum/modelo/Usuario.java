package br.com.alura.forum.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Usuario{
  
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;
  private String nome;
  private String email;
  private String senha;
  
  public Usuario(){
  }
  
  public Usuario(String nome,String email,String senha){
    this.nome=nome;
    this.email=email;
    this.senha=senha;
  }
  
  public Long getId(){
    return id;
  }
  
  public String getNome(){
    return nome;
  }
  
  public String getEmail(){
    return email;
  }
  
  public String getSenha(){
    return senha;
  }
  
  @Override
  public boolean equals(Object o){
    if(this==o) return true;
    if(o==null || getClass()!=o.getClass()) return false;
    Usuario usuario=(Usuario) o;
    return Objects.equals(id,usuario.id);
  }
  
  @Override
  public int hashCode(){
    return Objects.hash(id);
  }
}
