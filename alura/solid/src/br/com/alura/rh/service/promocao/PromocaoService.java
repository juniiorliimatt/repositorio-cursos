package br.com.alura.rh.service.promocao;

import br.com.alura.rh.ValidacaoException;
import br.com.alura.rh.model.Cargo;
import br.com.alura.rh.model.Funcionario;

public class PromocaoService{

  public void promover(Funcionario funcionario, boolean metabatida){
    var cargoAtual = funcionario.getDadosPessoais().getCargo();
    if(cargoAtual.equals(Cargo.GERENTE)){
      throw  new ValidacaoException("Gerentes não podem ser promovidos!");
    }

    if(metabatida){
      Cargo novoCargo = cargoAtual.getProximoCargo();
      funcionario.promover(novoCargo);
    } else {
      throw  new ValidacaoException("Funcionário não bateu a meta!");
    }
  }
}
