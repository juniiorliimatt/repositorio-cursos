package aplicativo;

import dominio.Pessoa;

import javax.persistence.Persistence;

public class DeletarPessoa{
  public static void main(String[] args){
    var emf = Persistence.createEntityManagerFactory("exemplo-jpa");
    var em = emf.createEntityManager();

    em.getTransaction().begin();
    em.remove(em.find(Pessoa.class, 5));
    em.getTransaction().commit();

    em.close();
    emf.close();
  }
}
