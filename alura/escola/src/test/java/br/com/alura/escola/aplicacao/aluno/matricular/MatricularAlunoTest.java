package br.com.alura.escola.aplicacao.aluno.matricular;

import br.com.alura.escola.aplicacao.matricular.MatricularAluno;
import br.com.alura.escola.aplicacao.matricular.MatricularAlunoDTO;
import br.com.alura.escola.dominio.aluno.Aluno;
import br.com.alura.escola.dominio.aluno.CPF;
import br.com.alura.escola.infra.aluno.RepositorioDeAlunosEmMemoria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MatricularAlunoTest{

	@Test
	void alunoDeveriaSerPersistido(){
		RepositorioDeAlunosEmMemoria repositorio=new RepositorioDeAlunosEmMemoria();
		MatricularAluno useCase=new MatricularAluno(repositorio);

		MatricularAlunoDTO dados=new MatricularAlunoDTO("Fulano", "123.456.789-00", "fulano@email.com");
		useCase.executa(dados);

		Aluno encontrado=repositorio.buscarPorCPF(new CPF("123.456.789-00"));
		Assertions.assertEquals("Fulano", encontrado.getNome());
		Assertions.assertEquals("123.456.789-00", encontrado.getCpf());
		Assertions.assertEquals("fulano@email.com", encontrado.getEmail());
	}
}
