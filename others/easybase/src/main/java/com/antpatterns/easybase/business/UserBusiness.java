package com.antpatterns.easybase.business;

import br.com.mv.equery.port.PageFilter;
import com.antpatterns.easybase.model.User;
import com.antpatterns.easybase.model.UserDTO;
import com.antpatterns.easybase.model.UsuarioResponse;
import com.antpatterns.easybase.repositorio.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class UserBusiness {

  @Autowired
  private UsuarioRepository repository;

  public User save(UserDTO userDTO) {
    return repository.save(userDTO.toUser(userDTO));
  }

  public Page<UsuarioResponse> get(Map<PageFilter, String> params, Pageable pageable) {
    return repository.buscarTodos(params, pageable);
  }
}
