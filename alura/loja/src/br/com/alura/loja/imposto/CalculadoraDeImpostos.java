package br.com.alura.loja.imposto;

import br.com.alura.loja.orcamento.Orcamento;

import java.math.BigDecimal;

/**
 * Usando Strategy, enum, interface.
 * @author Junior
 * @since 22/09/2021
 */
public class CalculadoraDeImpostos{

  public BigDecimal calcular(Orcamento orcamento, Imposto imposto){
    return imposto.calcular(orcamento);
  }
}
