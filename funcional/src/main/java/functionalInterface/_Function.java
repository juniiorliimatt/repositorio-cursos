package functionalInterface;

import java.util.function.BiFunction;
import java.util.function.Function;

public class _Function{
  public static void main(String[] args){

    int increment = incrementByOneFunction.apply(2);
    System.out.println(increment);

    int multiply = multiplyBy10Function.apply(increment);
    System.out.println(multiply);

    Function<Integer, Integer> addBy1AndThenMultiplyBy10 = incrementByOneFunction.andThen(multiplyBy10Function);
    System.out.println(addBy1AndThenMultiplyBy10.apply(1));

    System.out.println(incrementByOneAndMultiplyByFunction.apply(4, 10));

  }

  // Criando uma function
  static Function<Integer, Integer> incrementByOneFunction = number -> number++;
  static Function<Integer, Integer> multiplyBy10Function = number -> number * 10;
  static BiFunction<Integer, Integer, Integer> incrementByOneAndMultiplyByFunction =
    (numberToIncrementByOne, numberToMultiplyBy) -> (numberToIncrementByOne + 1) * numberToMultiplyBy;

}
