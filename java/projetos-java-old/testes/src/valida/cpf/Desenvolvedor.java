package valida.cpf;

import java.util.Scanner;
import java.util.logging.Logger;

public class Desenvolvedor {
  final Logger log = Logger.getLogger(Desenvolvedor.class.getName());
  private Integer matricula;
  private String nome;

  Desenvolvedor(Integer matricula, String nome) {
    this.matricula = matricula;
    this.nome = nome;
  }

  public static Desenvolvedor obterDados(Scanner scan, Logger log) {
    log.info("Digite seu nome: ");
    String nome = scan.next();
    log.info("Digite sua matricula: ");
    Integer matricula = scan.nextInt();

    return new Desenvolvedor(matricula, nome);
  }

  public static void info(Desenvolvedor dev, Logger log) {
    log.info("\n\nDesenvolvedor: " + dev.getNome() + ", Matricula: " + dev.getMatricula());
  }

  public Integer getMatricula() {
    return matricula;
  }

  public void setMatricula(Integer matricula) {
    this.matricula = matricula;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }
}
