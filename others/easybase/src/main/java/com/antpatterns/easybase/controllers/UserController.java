package com.antpatterns.easybase.controllers;

import br.com.mv.equery.port.PageFilter;
import com.antpatterns.easybase.business.UserBusiness;
import com.antpatterns.easybase.model.User;
import com.antpatterns.easybase.model.UserDTO;
import com.antpatterns.easybase.model.UsuarioResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/users")
public class UserController {

  @Autowired
  private UserBusiness business;

  @PostMapping
  public ResponseEntity<User> save(@Valid @RequestBody UserDTO userDTO) {
    business.save(userDTO);
    return ResponseEntity.status(HttpStatus.CREATED).build();
  }

  @GetMapping
  public ResponseEntity<Page<UsuarioResponse>> get(@RequestParam Map<PageFilter, String> params, Pageable pageable) {
    return ResponseEntity.ok(business.get(params, pageable));
  }
}
