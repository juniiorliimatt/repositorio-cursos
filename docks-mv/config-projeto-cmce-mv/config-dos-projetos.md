# Config. dos projetos

## CMCE consultas-exames, regulador leitos

instalar spring **versão do spring 3.9.12**

- executar no terminal pra pegar as dependências do nexus npm config set registry [http://nexus.mvfor.local/repository/npm-all/](http://nexus.mvfor.local/repository/npm-all/)
- instalar lombok no STS menu Help/install new software e colocar a url: [https://projectlombok.org/p2](https://projectlombok.org/p2)
- configurar variaveis de ambiente JAVA_HOME para apontar pro JDK!
- [config-dos-projetos/Untitled.png](config-dos-projetos/Untitled.png)  
- [config-dos-projetos/Untitled1.png](config-dos-projetos/Untitled1.png)

## Configurar jdk no STS

- Preferences > Installed Jre!
- [config-dos-projetos/Untitled2.png](config-dos-projetos/Untitled2.png)  

## Configurando nexus

no sts ir em WINDOWS > PREFERENCE > MAVEN > USER SETTINGS, na opção User settings  buscar o arquivo settings.xml (Esse arquivo deve ser salvo dentro da pasta .m2)

```xml
<?xml version="1.0" encoding="UTF-8"?>

<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0" 
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
  
  <pluginGroups></pluginGroups>

  <proxies></proxies>

  <servers>
    <server>
      <id>MVNexus</id>
      <username>deployment</username>
      <password>deployment123</password>
    </server>
  </servers>

  <mirrors>
    <mirror>
      <id>MVFOR</id>
      <name>MVFortaleza</name>
      <url>http://nexus.mvfor.local/repository/maven-public</url>
      <mirrorOf>*</mirrorOf>
    </mirror>
  </mirrors>
  
  <profiles>
    <profile>
      <id>NexusMVFOR</id>
      <activation>
       <activeByDefault>true</activeByDefault>
      </activation>

      <repositories>
        <repository>
          <id>releases</id>
          <url>http://nexus.mvfor.local/repository/maven-releases</url>
          <layout>default</layout>
          <releases>
            <enabled>true</enabled>
            <updatePolicy>always</updatePolicy>
          </releases>
          <snapshots>
            <enabled>false</enabled>
            <updatePolicy>always</updatePolicy>
          </snapshots>
        </repository>

        <repository>
          <id>snapshots</id>
          <url>http://nexus.mvfor.local/repository/maven-snapshots</url>
          <layout>default</layout>
          <releases>
            <enabled>false</enabled>
            <updatePolicy>always</updatePolicy>
          </releases>
          <snapshots>
            <enabled>true</enabled>
            <updatePolicy>always</updatePolicy>
          </snapshots>
        </repository>        

      </repositories>
    </profile>
  </profiles>

</settings>
```

## Front-end

repo: [http://git.mvfor.local/saude-publica/angular/regulacao/consultas-exames-ng](http://git.mvfor.local/saude-publica/angular/regulacao/consultas-exames-ng)

git clone [http://git.mvfor.local/saude-publica/angular/regulacao/consultas-exames-ng](http://git.mvfor.local/saude-publica/angular/regulacao/consultas-exames-ng).git

APAGAR o package-lock.json

- adicionar no path das variáveis de ambiente

    C:\Users\luis.filho-for\AppData\Roaming\npm\node_modules\@angular\cli\bin

- Para alternar entre os bancos no front: config.development.json
- Executar no powershell como admin antes de rodar o ng serve para permitir a execução dos scripts .ps1 remotos do projeto

    Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Unrestricted -Force;

- Configurar o arquivo proxy.conf.json na última e penúltima rota.

    ```bash
      "/mvautenticador-cas" : {
          "target": "http://tst-caxias.consulfarmasaude.com.br",
          "secure": false
        },
        "/mvsso" : {
          "target": "http://tst-caxias.consulfarmasaude.com.br",
          "secure": false
        }
    ```

- ng serve pra rodar o front
- Credênciais front Oracle(OAuth)

    Usuário: usercmcec

    Senha: 123

    Perfil recepção CMCE

    Hospital pompeia

- Credênciais front Postgres(CAS)

    Usuário: dalmir

    Senha: 123456

    Perfil recepção CMCE

    Hospital pompeia

- [http://localhost:4200/consultas-exames-api/logout](http://localhost:4200/consultas-exames-api/logout) para fazer logout

## Back-end

atual: [http://git.mvfor.local/saude-publica/api/regulacao/regulacao-parent](http://git.mvfor.local/saude-publica/api/regulacao/regulacao-parent)

antigo: [http://git.mvfor.local/saude-publica/api/saude-publica-html5-parent](http://git.mvfor.local/saude-publica/api/saude-publica-html5-parent)

```bash
git clone --recurse-submodules http://git.mvfor.local/saude-publica/api/regulacao/regulacao-parent[.git](http://git.mvfor.local/saude-publica/api/saude-publica-html5-parent.git)
git submodule update --init --recursive --remote
git submodule foreach --recursive git checkout release/1.82.7
git checkout release/1.82.7

mvn install na raiz do projeto
```

- clonar e importar no STS o controle-acesso-sso (NECESSÁRIO PARA EXECUTAR JUNTO QUANDO FOR RODAR O BANCO ORACLE)

    git clone [http://git.mvfor.local/saude-publica/api/geral/controle-acesso-sso](http://git.mvfor.local/saude-publica/api/geral/controle-acesso-sso).git

- Configurar os arquivos .yml dos projetos

    ![config-dos-projetos/Untitled3.png](config-dos-projetos/Untitled3.png)

- nas propriedades do jpa no arquivo application.yml adicionar a linha 9

    hibernate.temp.use_jdbc_metadata_defaults: false

    ![config-dos-projetos/Untitled4.png](config-dos-projetos/Untitled4.png)

- para executar o projeto, rodar o "consultas-exames-server-service" no Spring Tool Suite
- quando for usar o bd oracle tem que subir o "controle-acess-sso" junto
  - erros

    ![config-dos-projetos/Untitled5.png](config-dos-projetos/Untitled5.png)

## Sobre a utilização dos bancos

- Para usar o banco **Oracle**:

    Alterar o arquivo .yml do back-end a flag para true no campo **OAuth** e false para o CAS

- Para usar o banco **Postgres**

    Alterar o arquivo .yml do back-end a flag para true no campo **CAS** e false para o OAuth

QUANDO DESENVOLVER, TESTAR NO OAUTH E CAS, PORQUE ALGUNS HOSPITAIS USAM UM E OUTRO. TEM QUE VALIDAR NOS DOIS.

## Leitos (regulador)

## Regulador Front-end

repo: [http://git.mvfor.local/saude-publica/angular/regulacao/regulador-ng](http://git.mvfor.local/saude-publica/angular/regulacao/regulador-ng)

ng serve pra rodar o front

clonar com -b release/1.82.7

quando clonar o projeto comentar a linha _auth=${NPM_TOKEN} do .npmrc

Atualizar o projeto: git pull origin release/1.82.7

Usuário: userregc

Senha: 123

## Regulador Back-end

- mesmo back do CMCE, só que na pasta regulador-server-service

fechar projetos core, generic-server-business e generic-server-service pra conseguir subir regulador-server-service

- setar no application-development.yml
- url: jdbc:oracle:thin:@vick.mvfor.local:1521:db4
- ele não possui o cas(postgres) configurado, apenas Oracle

## APAC (Autorização de procedimento de alto custo)

Usado para os procedimentos de radioterapia, quimioterapia, etc...

No apac não usa a branch 1.82.7, é 1.82.0 tanto no back-end como front-end

## APAC Front-end

repo: [http://git.mvfor.local/saude-publica/components-js/regulacao/apac-front](http://git.mvfor.local/saude-publica/components-js/regulacao/apac-front)

git clone -b release/1.82.0 [http://git.mvfor.local/saude-publica/components-js/regulacao/apac-front](http://git.mvfor.local/saude-publica/components-js/regulacao/apac-front).git

Para instalar o projeto, rodar o install.sh

Mudar no arquivo env.json a profille de production para development

Para executar o projeto: npm run watch

[http://localhost:8399/](http://localhost:8399/)

Usuário: USERAPACC

Senha: 123

## APAC Back-end

 [http://git.mvfor.local/saude-publica/api/regulacao/apac](http://git.mvfor.local/saude-publica/api/regulacao/apac)

git clone -b release/1.82.0 [http://git.mvfor.local/saude-publica/api/regulacao/apac](http://git.mvfor.local/saude-publica/api/regulacao/apac).git

mvn install

OBRIGATÓRIO USAR O **controle-acesso-sso** TANTO PARA ORACLE COMO POSTGRES NO APAC

para subir o projeto, executar o apac-server-service

O banco postgres geralmente demora para subir. É preferível usar o Oracle pra desenvolver no apac e subir o postgres só para testar.

## Todos scripts dos projetos git - sysupdate

Usado para enviar os scripts alterados no banco de dados quando solicitado.

[http://git.mvfor.local/saude-publica/flex/sistemas-apoio/sysupdate.git](http://git.mvfor.local/saude-publica/flex/sistemas-apoio/sysupdate.git)

[voltar](../../README.md)
