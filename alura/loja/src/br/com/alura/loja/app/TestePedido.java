package br.com.alura.loja.app;

import br.com.alura.loja.pedido.GeraPedido;
import br.com.alura.loja.pedido.GerarPedidoHandler;
import br.com.alura.loja.pedido.acao.EnviarEmailPedido;
import br.com.alura.loja.pedido.acao.LogDePedido;
import br.com.alura.loja.pedido.acao.SalvarPedidoNoBancoDeDados;

import java.math.BigDecimal;
import java.util.Arrays;

public class TestePedido{
  public static void main(String[] args){
    String cliente="Junior";
    BigDecimal valorOrcamento=new BigDecimal("500.00");
    int quantidadeItens=5;

    GeraPedido gerador=new GeraPedido(cliente, valorOrcamento, quantidadeItens);
    GerarPedidoHandler handler=new GerarPedidoHandler(Arrays.asList(new SalvarPedidoNoBancoDeDados(),
        new EnviarEmailPedido(), new LogDePedido()));

    handler.excecute(gerador);
  }
}
