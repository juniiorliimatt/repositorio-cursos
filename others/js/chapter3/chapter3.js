function min(numberOne, numberTwo) {
  if (numberOne < numberTwo) return numberOne;
  return numberTwo;
}

console.log(min(0, -10));

function isEven(n) {
  if (n == 0) {
    return true;
  } else if (n == 1) {
    return false;
  } else if (n < 0) {
    return isEven(-n);
  } else {
    return isEven(n - 2);
  }
}

console.log(isEven(50));
console.log(isEven(75));
console.log(isEven(-1));

function countBs(bean) {
  var count = 0;
  for (var i = 0; i < bean.length; i++) {
    if (bean.charAt(i) == "B") {
      count++;
    }
  }
  return count;
}

function countChar(bean, s) {
  var count = 0;
  for (var i = 0; i < bean.length; i++) {
    if (bean.charAt(i) == s) {
      count++;
    }
  }
  return count;
}

console.log(countBs("BBC"));
console.log(countChar("kakkerlak", "k"));
