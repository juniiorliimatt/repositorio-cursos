package aplicativo;

import dominio.Pessoa;

import javax.persistence.Persistence;
import java.io.Serializable;

public class SalvarPessoa implements Serializable{
  public static void main(String[] args){
    var p1 = new Pessoa(null, "Carlos da Silva", "carlos@gmail.com");
    var p2 = new Pessoa(null, "Joaquim Torres", "joaquim@gmail.com");
    var p3 = new Pessoa(null, "Ana Maria", "ana@gmail.com");

    var emf = Persistence.createEntityManagerFactory("exemplo-jpa");
    var em = emf.createEntityManager();

    em.getTransaction().begin();
    em.persist(p1);
    em.persist(p2);
    em.persist(p3);
    em.getTransaction().commit();

    em.close();
    emf.close();
  }
}
